<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';
session_start();
if(isset($_SESSION['firstname'])){
    $firstname = $_SESSION['firstname'];
}
if(isset($_SESSION['lastname'])){
    $lastname = $_SESSION['lastname'];
}
if(isset($_SESSION['rank'])){
    $rank = $_SESSION['rank'];
}
if(isset($_SESSION['userID'])){
    $userid = $_SESSION['userID'];
}else{
    header('Location: index.php');
}
if(isset($_SESSION['Period'])){
    $period = $_SESSION['Period'];
}
$person = $_SESSION["User"];
$aa = 1;
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Questionare</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/questions.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <?php
    $data="none";
    if(isset($_POST["Submission"])){
        $data="Submission";
    }elseif (isset($_POST["Implementation"])) {
        $data="Implementation";
    }elseif (isset($_POST["IT"])) {
        $data="IT";
    }elseif (isset($_POST["Managment"])) {
        $data="Managment";
    }elseif (isset($_POST["Marketing"])) {
        $data="Marketing";
    }elseif (isset($_POST["Self"])) {
        $data="Self";
    }
    else{
        $data=$person->getTextDepartment()."Department";
    }    

    ?>
    <div class = "jumbo">
      <div class="jumbotron">
        <h1 class="display-6 headT"><?php echo $data?> Questionare</h1>
        <p class="display-4 subT">Rating 1 - 10 where 1 is the lowest score and 10 is the highest.</p>
        <h4 class="commT"> If you don't have an answer, please enter 0.</h4>
        <hr class="my-4">
    </div>
</div>
    <?php
        $_SESSION["Dep"] = $data;
        if($data=="Submission" ||$data=="Implementation" ||$data=="Managment" ||$data=="Marketing" ||$data=="IT"){

    ?>
    <div class="content">
        <form class="quest" action="test.php" method="post">
        <div class="tbl">
        <table>
            <tr>
                <th>Executives Questions</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]['execs']);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data]['execs'][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    
   ?>
    </div>
    <div class="tbl">
        <table>
            <tr>
                <th>Organisation Questions</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]['Organization']);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data]['Organization'][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    
   ?>
    </div>
    <div class="tbl">
        <table>
            <tr>
                <th>Cooperation Questions</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]['cooperation']);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data]['cooperation'][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    
   ?>
    </div>
    <div class="tbl">
        <table>
            <tr>
                <th>Contribution to IED'S Goals</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]['Contribution']);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data]['Contribution'][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    
   ?>
    </div>
    <?php
    if($data!="Managment"){
        ?>
         <div class="tbl">
        <table>
            <tr>
                <th>Work Quality</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]['WorkQuality']);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data]['WorkQuality'][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    
   ?>
    </div>

    <?php
    }
    ?>
    </div>
    <?php    
        }elseif($data == "Self"){
            ?>
            <div class="tbl">
        <form class="quest" action="test.php" method="post">
        <table>
            <tr>
                <th>Self Evaluation</th>
                <th>Answer</th>
            </tr>
        <?php
    $strJsonFileContents = file_get_contents("json/final.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$data]);$i++){
        echo "<tr>";
        echo "<td>".$questions[$data][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getFirstName().$data.$aa.'" min="1" max="10" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
       
    }else{
        echo '<div class="tbl">
        <form class="quest" action="test.php" method="post">
        <table>
            <tr>
                <th>Department Goal Evaluation</th>
                <th>Answer</th>
            </tr>';
             $strJsonFileContents = file_get_contents("json/goals.json");
    //var_dump($strJsonFileContents);
    $questions = json_decode($strJsonFileContents,true);
        
    for($i = 0;$i<sizeof($questions[$person->getTextDepartment()."Goals"]);$i++){
        echo "<tr>";
        echo "<td>".$questions[$person->getTextDepartment()."Goals"][$i]."</td>";
        echo '<td><input class="form-control" type="number" name ="'.$person->getTextDepartment().$aa.'" min="0" max="100" required></td>';
        $aa++;
            //echo $questions[$data]['execs'][$i]; 
         echo "</tr>";          
    }
    echo "</table>";
    }
    echo '<button type="submit" class="btn btn-lg btn-outline-light">Submit  <i class="fas fa-chevron-right"></i></button>';
    ?>
    </div>
     </form>


</body>
</html>
