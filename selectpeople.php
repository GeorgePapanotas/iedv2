<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';

session_start();
if(isset($_SESSION['User'])){
    $r =  $_SESSION['User'];
}else{
  header('Location: index.php');
}

$r->getLastname();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>People</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/questions.css">
    <link rel="stylesheet" href="css/selectppl.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
    <div class = "headingR">
      <div class="jumbotron">
        <h1 class="display-4">Period Creation Suite</h1>
        <p class="lead">To create a new period please fill the form below.</p>
        <hr class="my-4">
        <p>Select the people, set the name, kickoff/conclusion dates and click submit.</p>
    </div>
</div>

<div class = "form">
  <form action="" method="post">

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Period Name</span>
      </div>
      <input type="text" id="text-box" name = "pName" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
  </div>
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Kickoff Date</span>
  </div>
  <input type="text" name = "startDate" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Conclusion Date</span>
  </div>
  <input type="text" name = "endDate" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
</div>
<div class = "btn_cont">
    <div class="row">
        <div class="col-lg-6 col-md-12">
          <button type="button" id = "slcAll" class="btn btn-outline-light"><i class="fas fa-check-circle"></i> Select All</button>
      </div>
      <div class="col-lg-6 col-md-12">
          <button type="button" id ="slcNone" disabled="true" class="btn btn-outline-light"><i class="fas fa-times"></i> Deselect All</button>
      </div>
  </div>
</div>
<legend>People</legend>      
<div class = "Box">
    <?php
    echo '<div class="row">
    <div class="col-lg-6 col-md-12">';
    $ppl = $r->getAllUsers();
    $i=0;
    while($i<sizeof($ppl)/2){
        if($ppl[$i][4]==1){
          echo '<div class="form-check">
          <input class="form-check-input check" type="checkbox" onclick="calc();" name = "selected[]" value="'.$ppl[$i][0].'" id="'.$ppl[$i][2].'">
          <label class="form-check-label label" for="'.$ppl[$i][2].'">
          '.$ppl[$i][1].' '.$ppl[$i][2].'
          </label>
          </div>
          <br>
          <hr>';
      }
      
      $i++;
  }
  echo "</div>";
  echo ' <div class="col-lg-6 col-md-12">';
  while($i<sizeof($ppl)){
    if($ppl[$i][4]==1){
      echo '<div class="form-check">
      <input class="form-check-input check" type="checkbox" onclick="calc();" name = "selected[]" value="'.$ppl[$i][0].'" id="'.$ppl[$i][2].'">
      <label class="form-check-label label" for="'.$ppl[$i][2].'">
      '.$ppl[$i][1].' '.$ppl[$i][2].'
      </label>
      </div>
      <br>
      <hr>';
  }
  $i++;
}
echo '</div>
</div>';

?>

</div>
<br>
<button type="submit" class="btn btn-lg btn-outline-light">Submit  <i class="fas fa-chevron-right"></i></button>
</form>
</div>
<?php
if(!isset($_POST['selected'])){
  $participants = array();
}else{
  $participants = $_POST['selected'];
}

if(empty($participants))
{
    echo("You didn't select any participants.");
}
else
{

    $par = '';
    foreach($participants as $p){
        $par = $par.','.$p;
    }
    $par  = substr($par, 1);
    $r->createPeriod($par,$_POST["pName"],$_POST["startDate"],$_POST["endDate"]);
}
?>
</body>
<script type="text/javascript">
    var m = document.getElementById("slcAll").addEventListener("click", function(){
      var x = document.getElementsByClassName("check");
      var z = document.getElementById("slcNone");
      for(var i = 0;i<x.length;i++){
        x[i].checked = true;
    }
    this.disabled = 'disabled';
    z.disabled = false;
});

    document.getElementById("slcNone").addEventListener("click", function(){
      var x = document.getElementsByClassName("check");
      var z = document.getElementById("slcAll");
      for(var i = 0;i<x.length;i++){
        x[i].checked = false;
    }
    this.disabled = true;
    z.disabled = false;
});

    function calc(){
      var x = document.getElementsByClassName("check");
      var unchecked = [];
      var checked = [];
      for(var i = 0;i<x.length;i++){
        if(x[i].checked == false){
           unchecked.push(x[i]);
       }else{
           checked.push(x[i]);
       }
   }

   if(unchecked.length>0 && checked.length>0){
    document.getElementById("slcNone").disabled = false;
    document.getElementById("slcAll").disabled = false;
}else if(unchecked.length == x.length){
    document.getElementById("slcNone").disabled = true;
    document.getElementById("slcAll").disabled = false;
}else if(checked.length == x.length){
    document.getElementById("slcNone").disabled = false;
    document.getElementById("slcAll").disabled = true;
}
      // if (this.checked){
      //    document.getElementById('slcNone').disabled = false;
      // }
  }
</script>
</html>
