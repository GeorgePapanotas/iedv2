<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
session_start();
if(isset($_SESSION['firstname'])){
  $firstname = $_SESSION['firstname'];
}
if(isset($_SESSION['lastname'])){
  $lastname = $_SESSION['lastname'];
}
if(isset($_SESSION['rank'])){
  $rank = $_SESSION['rank'];
}
if(isset($_SESSION['userID'])){
  $userid = $_SESSION['userID'];
}else{
  header('Location: index.php');
}
$person = new User($userid,$firstname,$lastname,$rank);
$period = $person->getLastPeriod();

$_SESSION['Period'] = $period;
$_SESSION['User'] = $person;
    //$person->getPeriodName($period);

$photo = $person->loadPhoto();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Main</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style media="screen" type="text/css">


    .info{
      padding-bottom: 40px;
    }

    .content{
      padding-top: 3rem;
      text-align: center;
    }

    .btn{
      margin-bottom:15px;
      width:100%;
    }

    .wlcm{
      color: #464646;
    }

    hr{
      width:75%;
    }

    h3{
      color: #ffffff;
      /*text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;*/
    }

    .ic1 {
      position: relative;
      top: 0;
      right: 5px;
    }

    .ic2{
      position: relative;
      top: 0px;
      right: 19px;
    }

    .ic3{
      position: relative;
      top: 0px;
      right: 50px;
    }

    .ic4{
      position: relative;
      top: 0px;
      right: 40px;
    }

    .ic5{
      position: relative;
      top: 0px;
      right: 40px;
    }

    form{
      width:100%;
    }

    .title{
      color:white;
    }


  </style>
</head>
<body>
  <div class="heading">
    <div class="row">
      <div class="d-none d-md-block col-lg-6 col-md-12 image">
        <img src="https://ied.eu/entre_files/child-theme/full-logo-text-white.png" alt="dog-profile" class="header-img">
      </div>
      <div class="col-lg-6 col-md-12 title">
        <h3>Πρόγραμμα Aξιολόγησης Προσωπικού</h3>
      </div>
    </div>
  </div>
  <hr/>
  <div class="content">
    <div class = row>
      <div class = "col-lg-6 col-md-12 upper">
        <?php
        echo '<img class = "person" src="'.$photo.'">';
        ?>
        
        <div class = "person-info centered">
          <h1 class = "info"><?php echo $firstname.' '.$lastname?></h1>
          <h2 class = "info"><?php echo $person->getUserPosition($person->getPosition())[0][0] ?></h2>
          <h2 class = "info"><?php echo $person->getTextDepartment() ?></h2>
        </div>
      </div>
      <div class = "col-lg-6 col-md-12">
        <div class="btn-group-vertical test">
          <?php
          try{
            if($person->checkINPeriod($person->getUserid())){
              echo '<form class="start" action="questionares.php" method="post">
              <button type="submit" class="btn btn-lg btn-outline-light" disabled><i class="fas fa-check-circle ic1"></i> Start Answering</button>
              </form>';
            }else{
              echo '<form class="start" action="questionares.php" method="post">
              <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-poll ic1"></i> Start Answering</button>
              </form>';
            }
          }catch(Exception $e){
            echo '<h6 style ="width:70%;margin:auto;padding-bottom:10px;">No periods in the database. Please contact the IT Manager.</h6>';
          }

          ?>

          <form class="results" action="results.php" method="post">
            <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-bullseye ic3"></i> Results</button>
          </form>
          <?php
          if($person->isAdmin()){
            echo '<form class="settings" action="settings.php" method="post">
            <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-sliders-h ic4"></i> Settings</button>
            </form>';
          }
          ?>            
          <form class="logout" action="index.php" method="post">
            <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-door-open ic5"></i> Log Out</button>
          </form>
        </div>
      </div>
    </div>

  </body>
  </html>
