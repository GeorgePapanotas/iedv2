<?php
// This class is used to render questions drawn from the database into the template.

class questions extends dbh{
  private $QText;
  private $QID;
  private $QuestID;

  public function setQText($QText){
    $this->QText = $QText;
  }
  public function getQText(){
    return $this->QText;
  }
  public function setQID($QID){
    $this->QID = $QID;
  }
  public function getQID(){
    return $this->QID;
  }
  public function setQuestID($QuestID){
    $this->QuestID = $QuestID;
  }
  public function getQuestID(){
    return $this->QuestID;
  }

  // Given the text this method creates the body of the question. It has inline style sheets cause it refused to work otherwise.
  // Considering the architecture I followed, it has to get modified to assign the qid drawn from the database to each question
  // for storing purposes.
  // By modified, I mean instead of passing $text, working on $this
  public function constructTemplateBody($person,$qid){
    if($qid === 1 || $qid === 2){
      echo '
      <div class="form-group form-inline question_container">
      <label for="'.$person.$this->getQID().'">'.$this->getQText().'</label>
      <input type="number" name = "'.$person.$this->getQID().'" class="form-control box" id="'.$person.$this->getQID().'" placeholder="Answer"  required min="0" max="10" style = "
      margin-right: 70px;
      margin-left: auto;
      background-color:#f8fdff;
      border-radius: 25px;
      width:15%;
      ">
      </div>
      <hr/>';
    }else{
      echo '
      <div class="form-group form-inline question_container">
      <label for="'.$person.$this->getQID().'">'.$this->getQText().'</label>
      <input type="number" name = "'.$person.$this->getQID().'" class="form-control box" id="'.$person.$this->getQID().'" placeholder="Answer"  required style = "
      margin-right: 70px;
      margin-left: auto;
      background-color:#f8fdff;
      border-radius: 25px;
      ">
      </div>
      <hr/>';
    }
    
  }

// Same logic, passing the data, it constructs the header. The QID is responsible for rendering the orrect prompt.
  public function constructTemplateHeader($firstname,$lastname,$position,$QID){
    $prompt;
    switch($QID){
      case 1:
      $prompt = '1 - Colleague Evaluation : <span class="coligue_name">'.$firstname.' '.$lastname.'</span> '.$position;
      break;
      case 2:
      $prompt = '2 - Self Evaluation';
      break;
      case 4:
      $prompt = '3 - Teamwork Logged In Hours (percentage) for : <span class="coligue_name">'.$firstname.' '.$lastname.'</span> '.$position;
      break;
      case 5:
      $prompt = '4 - Department Goals';
      break;
      case 6:
      $prompt = "1 - IT Department Evaluation";
      break;
    }
    echo '<div class="card">
    <div class="card-header" id="headingOne">
    <h5 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#'.$lastname.''.$QID.'" aria-expanded="false" aria-controls="'.$lastname.'">
    '.$prompt.'
    </button>
    </h5>
    </div>
    <div id="'.$lastname.''.$QID.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
    <div class="card-body">';
  }

}

?>
