<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
session_start();
if(isset($_SESSION['firstname'])){
  $firstname = $_SESSION['firstname'];
}
if(isset($_SESSION['lastname'])){
  $lastname = $_SESSION['lastname'];
}
if(isset($_SESSION['rank'])){
  $rank = $_SESSION['rank'];
}
if(isset($_SESSION['userID'])){
  $userid = $_SESSION['userID'];
}else{
  header('Location: index.php');
}
$person = new User($userid,$firstname,$lastname,$rank);
$period = $person->getLastPeriod();

$_SESSION['Period'] = $period;
$_SESSION['User'] = $person;

$deps = ["IT","Marketing","Implementation","Managment","Submission","Self"];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Your Questionares</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link rel="stylesheet" href="css/styles.css">
     <link rel="stylesheet" href="css/questionares.css">
</head>
<body>
	 <div class = "jumbo">
      <div class="jumbotron">
        <h1 class="display-6 headT">Questionares</h1>
        <p class="display-4 subT">These are the questionares you have to answer.</p>
        <h4 class="commT">Note that each questionare is answerd seperatly.</h4>
        <hr class="my-4">
    </div>
</div>
    <div class="content">
    	<div class="questionares" class="btn-group-vertical">
    		<table>
    			<tr>
    				<th><h4>Questionare</h4></th>
    				<th><h4>Fullfiled</h4></th>
    				<th><h4>Action</h4></th>
    			</tr>
    			<?php
    				$dep = $person->getTextDepartment();
    				$deps=array_diff($deps, [$dep]);
    				foreach($deps as $d){
    					if($person->checkAnswered1($period,$d)){
    						echo '<tr>
    				<td>'.$d.' Questionare</td>
    				<td class="fyes"><i class="fas fa-check-circle"></i></td>
    				<td><form class="back" action="questions.php" method="post">
  						<button id="back" name="'.$d.'" value = "1" type="submit" class="btn btn-secondary disabled">Answer!</button>
  						</form></td>
    				</tr>';
    					}else{
    						echo '<tr>
    				<td>'.$d.' Questionare</td>
    				<td class="fno"><i class="far sms frown fa-circle"></i></td>
    				<td><form class="back" action="questions.php" method="post">
  						<button id="back" name="'.$d.'" value = "1" type="submit" class="btn btn-light">Answer!</button>
  						</form></td>
    				</tr>';
    					}
    					

    				}

    			?>
    			<!-- <tr>
    				<td>Implementation Questionare</td>
    				<td class="fyes"><i class="fas fa-check-circle"></i></td>
    				<td><button class="btn btn-secondary disabled">Answer!</button></td>
    			</tr> -->
    			<!-- <tr>
    				<td>Self Questionare</td>
    				<td class="fno"><i class="far sms frown fa-circle"></i></td>
    				<td><form class="back" action="questions.php" method="post">
  						<button id="back" name="Self" value = "1" type="submit" class="btn btn-light">Answer!</button>
  						</form></td>
    				</tr>
    			</tr> -->
    			<?php
    				if(($person->isSupervisor()) && !($person->getLastname()=="Siakavelis")){
    					if($person->checkOtherSupervisor($period)){
    						echo '<tr>
    				<td>Department Goals Questionare</td>
    				<td class="fyes"><i class="fas fa-check-circle"></i></td>
    				<td><form class="back" action="questions.php" method="post">
  						<button id="back" name="Department" value = "1" type="submit" class="btn btn-secondary disabled">Answer!</button>
  						</form></td>
    				</tr>';
    					}else{
    						echo '<tr>
    				<td>Department Goals Questionare</td>
    				<td class="fno"><i class="far sms frown fa-circle"></i></td>
    				<td><form class="back" action="questions.php" method="post">
  						<button id="back" name="Department" value = "1" type="submit" class="btn btn-light">Answer!</button>
  						</form></td>
    				</tr>';
    					}
    					
    				}
    			?>

    			
    		</table>
    	</div>
    	 <form class="back" action="main.php" method="post">
  <button id="back" type="submit" class="btn btn-lg btn-light">Back to Main</button>
  </form>
    </div>
<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
  
  <script type="text/javascript">
  	$(".frown").mouseenter(function(){
  		$(this).addClass("fa-check-circle");
  		$(this).parent().addClass("fyes");
  		$(this).removeClass("fa-circle");
  		$(this).parent().removeClass("fno");
  	})

  	$(".frown").mouseleave(function(){
  		$(this).addClass("fa-circle");
  		$(this).parent().removeClass("fyes");
  		$(this).removeClass("fa-check-circle");
  		$(this).parent().addClass("fno");

  	})

 
  			console.log($("td:nth-child(n+2)").children().hasClass("fa-check-circle"));
  	

  </script>
</body>
</html>