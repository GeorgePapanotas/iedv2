<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';
session_start();
if($_SESSION['User']){
  $user = $_SESSION['User'];
}else{
  header('Location: index.php');
}


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Settings</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/selectppl.css">
  <link rel="stylesheet" href="css/results.css">
  <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body style = "text-align: center;">
  
	<?php
  if($user->isAdmin()){
    echo '<div class = "headingR">
    <div class="jumbotron">
    <h1 class="display-4">Settings Panel</h1>
    <p class="lead">Hello '.$user->getFirstname().'. As and admin you can now create periods</p>
    <hr class="my-4">
    <p class="lead">
    <form class="start" action="selectpeople.php" method="post">
    <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-poll ic1"></i> Period Creation Suite</button>
    </form>
    </p>
    </div>
    </div>';
    echo '<form class="start" action="main.php" method="post">
    <button type="submit" class="btn btn-lg btn-outline-light"><i class="fas fa-chevron-left"></i> Back to Main</button>
    </form>';
  }else{
    echo '<div class = "headingR">
    <div class="jumbotron">
    <h1 class="display-4">Settings Panel</h1>
    <p class="lead">This is a placeholder page. More functionality will be added later!</p>
    <hr class="my-4">
    </div>
    <p class="lead">
    <a class="btn btn-outline-light btn-lg" href="main.php" role="button"><i class="fas fa-chevron-left"></i> Back to Main</a>
    </p>
    </div>';
  }
  ?>
</body>
</html>