<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';
session_start();
if(isset($_SESSION['firstname'])){
  $firstname = $_SESSION['firstname'];
}
if(isset($_SESSION['lastname'])){
  $lastname = $_SESSION['lastname'];
}
if(isset($_SESSION['rank'])){
  $rank = $_SESSION['rank'];
}
if(isset($_SESSION['userID'])){
  $userid = $_SESSION['userID'];
}else{
  header('Location: index.php');
}

$LoggedUser = new User($userid,$firstname,$lastname,$rank);
$periods = $LoggedUser->getPeriodData();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Results</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/questions.css">
  <link rel="stylesheet" href="css/results.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    body{
      color:white;
    }
    .names_cont{
      background-color: rgba(59, 47, 47, 0.78);
      width:60%;
      margin:auto;
      padding: 1px;
      border-radius: 10px;
    }

    .ncomp{
      margin:auto;
      padding-top:3%;
      padding-bottom:2%;
      text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    }

    td{
      color:white;
    }
    table{
      background-color: rgba(59, 47, 47, 0.78);
    }
    caption{
      color:white;
    }
  </style>
</head>
<body>
  <div class = "headingR">
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">Results</h1>
        <p class="lead">Please select a period to display it's results!</p>
      </div>
    </div>
  </div>
  <form action = "" method = "POST">
    <select class="form-control perSel" name = "perSel" onchange="this.form.submit()">
      <option class = "opt">Select Period</option>
      <?php
      foreach($periods as $p){
        echo '<option class = "opt">'.$p['Title'].'</option>';
      }
      ?>
    </select>
  </form>

  <?php
  function getSupervisorAnswers($period){

    $supers = array(3,8,11,13,14,18,19);
    if (($key = array_search($LoggedUser->getUserid(), $supers)) !== false) {
      unset($supers[$key]);
    }
    $m = '';
    foreach($supers as $s){
      $m= $m.','.$s;
    }

    $stmt = $this->connect()->query("SELECT sum(result) FROM answers WHERE target = 8 and period = ".$period." and userid in(".substr($m,1).") and questid in (select questid from questions where qid = ".$this->getQids()." )");
    $res = $stmt->fetch();

    $ret = $res[0];
    return $ret;

  }

  if(isset($_POST['perSel'])){
    $period = $_POST['perSel'];
    if($LoggedUser->checkAllAnswerd($LoggedUser->getPeriodId($period))){
      $ppl = $LoggedUser->getParticipants($LoggedUser->getPeriodId($period));

      echo '<table class="table table-hover table-bordered table-sm">
      <caption>Results for: '.$period.'</caption>
      <thead>
      <tr class = "table-info">
      <th scope="col">User ID</th>
      <th scope="col">Name</th>
      <th scope="col">Active</th>
      <th scope="col">Colleagues</th>
      <th scope="col">Self</th>
      <th scope="col">Teamwork</th>
      <th scope="col">Goals (Department)</th>
      <th scope="col">Goals (Person)</th>
      <th scope="col" class = "bg-success total">Total</th>
      </tr>
      </thead>
      <tbody>';


      $tr = $LoggedUser->getRersultsWithSort($LoggedUser->getPeriodId($period));
      foreach($tr as $p){
        $name = $LoggedUser->getDataByID($p[7]);
        echo '<tr>
        <th scope="row" style = "color:white;">'.$p[7].'</th>
        <td class = "name">'.$name[0].' '.$name[1].'</td>
        <td>'.($name[2] == 1 ? 'yes' : 'no').'</td>
        <td>'.$p[0].'%</td>
        <td>'.$p[1].'%</td>
        <td>'.$p[2].'%</td>
        <td>'.$p[3].'%</td>
        <td>'.round($p[4],0).'%</td>
        <td class = "bg-success total">'.($p[5]).'</td>
        </tr>';



      }

      echo '</tbody>
      </table>';
    }else{
      echo '<h3 class = "ncomp"><i class="fas fa-exclamation-triangle"></i> The period you have chosen is not yet complete! <i class="fas fa-exclamation-triangle"></i> </h3>';
      $LoggedUser->getNotAnswered($LoggedUser->getPeriodId($period));
    }

  }
  echo '<form class="back" action="main.php" method="post">
  <button type="submit" class="btn btn-lg btn-light"><i class="fas fa-chevron-left"></i> Back to Main</button>
  </form>';
  ?>
</body>
</html>
