<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
?>
<html>
<body>
  <?php
  $username = $_POST["username"];
  $pass = $_POST["password"];
  $object = new User(1,1,1,1);
  $cred = $object->authenticate($username,$pass);
  echo $cred;
  session_start();
  if($cred != -1){

    $person = $object->getUserData($cred);


    $_SESSION['firstname'] = $person['firstname'];
    $_SESSION['lastname'] = $person['lastname'];
    $_SESSION['rank'] = $person['rank1'];
    $_SESSION['userID'] = $cred;
    header('Location: main.php');
  }else{
    $_SESSION['login'] = "You have entered invalid username or password.";
    header('Location: index.php');
  }
  ?>
</body>
</html>
