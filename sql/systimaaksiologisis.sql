-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2019 at 01:17 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `systimaaksiologisis`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `userID` int(11) NOT NULL,
  `QuestID` int(11) NOT NULL,
  `target` varchar(20) CHARACTER SET utf8 NOT NULL,
  `period` int(11) NOT NULL,
  `Ans1` double NOT NULL,
  `Result` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

CREATE TABLE `credentials` (
  `username` varchar(36) NOT NULL,
  `password1` varchar(128) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

INSERT INTO `credentials` (`username`, `password1`, `userID`) VALUES
('AnnaK', '$2y$10$8LOSpOJHtAYB2mlhxhhYguLeB63n4hSutUhvQmJPE4O25XeHB12GC', 1),
('MariaD', '$2y$10$8U2CW7RkSudxCZVstXdv2.EqCEabn0BDM/utzs7jyUD5RU9h9axXC', 2),
('PanagiotisK', '$2y$10$gWf07noAQmZNkMagGGYBGeIYy0i.4z16ji6iaUSD/YAPUTGzvAWIy', 3),
('StellaI', '$2y$10$Fa0CxZqiiWDndH6yWHyag.GHxt7/QLCJqf3diTHQPkycYXDQgh7sq', 4),
('JuliaB', '$2y$10$mAQTiq7IWtwaOBrzhXx60uAUd/.Gj7um35R0o4ErfUgnUjloXyeeG', 5),
('MariaS', '$2y$10$SVu15eGj7kLQnbwoGnF1HuKCb6HLUvPE9ium6BRKorLmn4Y27yuDi', 6),
('AlexandraX', '$2y$10$d6yju79QVaDeMij6YCdjnectL44r/HQSzfXZTNgNKLdiCo7L1MMS2', 7),
('DimitrisS', '$2y$10$s2l0WHc.Wi/jco3d5023Ve3SlNlagmL0ldBO3a8Hc2GPX5aLJ3juC', 8),
('KaterinaP', '$2y$10$7kQ1s9tdnS4fEhOtJPYZruJmBlLCKJc6QVPGEPvRUaiGrT0TDaYxi', 9),
('AggelikiM', '$2y$10$28MmeQfJ4ALPWFkgRewB6uMtpzgWXG5y4guN.jiIc80HRtlV9g7eG', 10),
('TasosV', '$2y$10$6iqrd6PxiGL1y25oCIlZlODYUXdwRPgjnwhhsdbqb6S1xNplMce8y', 11),
('KostasG', '$2y$10$S9WSdwgg0KC2lsAV4VMRYuMGYAmUT8n8rk3lEAsxk60Wfv.EWxjKO', 12),
('ThodorisA', '$2y$10$wbfP22IFbjVGO3otp09rn.790eesN35PDTPD6inuecWJ0fZmPm8VW', 13),
('ThodorisG', '$2y$10$O2cE4MnJSAaUie2E2B0v2e3GWUFel/IkVu5E9eyyqLKFEnHs2.MeC', 14),
('EvaB', '$2y$10$hQuaE1p5SL/pVwaexAW4Kud4bnTtQ92gG2r5g5IdKMfCnxHUf4vt6', 15),
('KaterinaP', '$2y$10$nBWK8u3RfC1b2X5IfRAaqetCU/bo8DNkaQqw/sYlHbKMjBdKd/kNu', 16),
('ElisavetPav', '$2y$10$zlkFzsQRMcZPwp.K9dR3r.TlNRDpN2643BKj.xyqe5k3Ax/8.x3iK', 17),
('MixalisL', '$2y$10$q9TSxnLgPJYAaFLBa.H8uO7uMj2drYnw9fp0y4ssmu7/FPjd9UyA.', 18),
('PanosP', '$2y$10$FDY8jlKvHKGmrAbq7/aTgOT/jExodpBmXMOwFe3O3ICwKBKHYpQy.', 19),
('SpirosV', '$2y$10$rpQWcCDdU0xpOiBeAHDgV.kBEdw3.eW3.btMxI0bG4mYBYeNNfEVe', 20),
('VeniaS', '$2y$10$DEvwTKe.PDcA3jHHP.m6nOB0flrn.jcVgh.LjLkLMXktbhK50EEBO', 21),
('AnastasiaS', '$2y$10$UcJNTP5fpLEFhR0Ihy2K.uG0pwujclPvw83kdshmghy6w9M8ojzQS', 22),
('LeonidasS', '$2y$10$bwAYeO7qm8VlTouzH79QXOm7NYH9NKYhIxMyFTImrTJl.OYN6uCFe', 23),
('Tester', '$2y$10$74vN8ljWtMh6r2LdGLCrD.xXLpJigvcZvUktPHGJlhZ.USTRM5pCm', 24),
('VickyS', '$2y$10$Wl.XXr9EydyHt1f9VPkp..CM0yEM4hIs6WG7uZ0qMJOD10hoyefom', 25),
('EugeniaK', '$2y$10$OR9LMeSWI8QWRVR2l/OCIemiwcqaGR7eCpAeeBkvZE4s5wVyfaIQO', 26);

-- --------------------------------------------------------

--
-- Table structure for table `ieddepartment`
--

CREATE TABLE `ieddepartment` (
  `DeptID` int(11) NOT NULL,
  `Title` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ieddepartment`
--

INSERT INTO `ieddepartment` (`DeptID`, `Title`) VALUES
(1, 'Implementation'),
(2, 'Submission'),
(3, 'IT'),
(4, 'Marketing'),
(5, 'Managment');

-- --------------------------------------------------------

--
-- Table structure for table `ieduser`
--

CREATE TABLE `ieduser` (
  `userID` int(11) NOT NULL,
  `firstname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `rank1` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `InactiveDate` date NOT NULL,
  `Admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ieduser`
--

INSERT INTO `ieduser` (`userID`, `firstname`, `lastname`, `rank1`, `Active`, `InactiveDate`, `Admin`) VALUES
(1, 'Anna', 'Koronioti', 1, 1, '0000-00-00', 0),
(2, 'Maria', 'Dalakoura', 1, 1, '0000-00-00', 0),
(3, 'Panagiotis', 'Koutoudis', 2, 1, '0000-00-00', 0),
(4, 'Stella', 'Ioannou', 1, 1, '0000-00-00', 0),
(5, 'Julia', 'Bahushi', 1, 1, '0000-00-00', 0),
(6, 'Maria', 'Skoufi', 1, 1, '0000-00-00', 0),
(7, 'Alexandra', 'Chimona', 1, 1, '0000-00-00', 0),
(8, 'Dimitris', 'Siakavelis', 4, 1, '0000-00-00', 1),
(9, 'Katerina', 'Pariza', 3, 1, '0000-00-00', 0),
(10, 'Aggeliki', 'Mantse', 5, 1, '0000-00-00', 0),
(11, 'Tasos', 'Vasileiadis', 6, 1, '0000-00-00', 0),
(12, 'Kostas', 'Grigoriou', 15, 1, '0000-00-00', 0),
(13, 'Thodoris', 'Alexandrou', 10, 1, '0000-00-00', 0),
(14, 'Thodoris', 'Giaouzis', 10, 0, '0000-00-00', 0),
(15, 'Eva', 'Batzogianni', 9, 1, '0000-00-00', 0),
(16, 'Katerina', 'Pouspourika', 8, 1, '0000-00-00', 0),
(17, 'Elisabeth', 'Pavlitsa', 7, 1, '0000-00-00', 0),
(18, 'Mike', 'Lagos', 13, 1, '0000-00-00', 0),
(19, 'Panos', 'Petsas', 14, 1, '0000-00-00', 0),
(20, 'Spiros', 'Voulgaris', 11, 1, '0000-00-00', 0),
(21, 'Venia', 'Soumpenioti', 12, 1, '0000-00-00', 0),
(22, 'Anastasia', 'Simaioforidou', 12, 1, '0000-00-00', 0),
(23, 'Leonidas', 'Somakos', 12, 1, '0000-00-00', 0),
(24, 'Tester George', 'Tester', 4, 0, '0000-00-00', 0),
(25, 'Vicky', 'Stathi', 3, 1, '0000-00-00', 0),
(26, 'Evgenia', 'Kosta', 1, 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE `periods` (
  `PeriodID` int(11) NOT NULL,
  `Participants` text CHARACTER SET utf8 NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Title` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `positionID` int(11) NOT NULL,
  `Title` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`positionID`, `Title`) VALUES
(1, 'Project Manager'),
(2, 'Supervisor - Lead Project Manager'),
(3, 'Designer'),
(4, 'Supervisor - Developer'),
(5, 'Secretary'),
(6, 'Supervisor - CEO'),
(7, 'Content Manager'),
(8, 'SEO Expert Copyrighter'),
(9, 'Social Media Manager'),
(10, 'Supervisor - Marketing and Innovation Manager'),
(11, 'Fundrising Network Assistant'),
(12, 'Proposal Writer'),
(13, 'Supervisor - Fundrising Network Manager'),
(14, 'Supervisor - Sub Planner'),
(15, 'Accountant');

-- --------------------------------------------------------

--
-- Table structure for table `questionares`
--

CREATE TABLE `questionares` (
  `QID` int(11) NOT NULL,
  `QType` varchar(60) NOT NULL,
  `QSubtype` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionares`
--

INSERT INTO `questionares` (`QID`, `QType`, `QSubtype`) VALUES
(1, '1', 'Project Manager'),
(2, '1', 'Supervisor - Lead Project Manager'),
(3, '1', 'All'),
(4, '1', 'Designer'),
(5, '1', 'Supervisor - Developer'),
(6, '1', 'Secretary'),
(7, '1', 'Supervisor - CEO'),
(8, '1', 'Content Manager'),
(9, '1', 'SEO Expert Copyrighter'),
(10, '1', 'Social Media Manager'),
(11, '1', 'Supervisor - Marketing and Innovation Manager'),
(12, '1', 'Fundrising Network Assistant'),
(13, '1', 'Proposal Writer'),
(14, '1', 'Supervisor - Fundrising Network Manager'),
(15, '1', 'Supervisor - Sub Planner'),
(16, '2', 'Self Evaluation'),
(17, '4', 'Teamwork'),
(18, '5', 'Implementation Goals'),
(19, '5', 'IT Goals'),
(20, '5', 'Managment Goals'),
(21, '5', 'Marketing Goals'),
(22, '5', 'Submission Goals');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `QuestID` int(11) NOT NULL,
  `QText` text NOT NULL,
  `QID` int(11) NOT NULL,
  `Multiplier` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`QuestID`, `QText`, `QID`, `Multiplier`) VALUES
(1, 'Quality of web development', 3, 1),
(2, 'Speed of web development', 3, 1),
(3, 'Graphic development quality / appearance', 3, 1),
(4, 'Graphic development speed', 3, 1),
(5, 'User support response time', 3, 1.5),
(6, 'User support know-how', 3, 1.5),
(7, 'User support to third parties or others', 3, 1),
(8, 'Innovation and new ideas', 3, 1),
(9, 'New systems developed', 3, 1),
(10, 'Attempt for individual improvement and new knowledge', 4, 0.4),
(11, 'Keeps working hours', 4, 0.5),
(12, 'Knowledge of the function of their department', 4, 0.3),
(13, 'Knowledge of iED', 4, 0.5),
(14, 'Flexibility and adaptability to change', 4, 0.5),
(15, 'Taking responsibility', 4, 0.5),
(16, 'Problem Solving Capability', 4, 0.6),
(17, 'Ability to work under pressure', 4, 0.6),
(18, 'Collaboration/Communication with colleagues and/or external partners', 4, 0.3),
(19, 'Interested, excited and willing to work', 4, 0.5),
(20, 'Accepts advice and instructions', 4, 0.6),
(21, 'General Organization', 4, 0.3),
(22, 'Ability to perform work with precision', 4, 0.6),
(23, 'Ability to perform work consistently', 4, 0.6),
(24, 'Knowledge of trends / hot topics / designing', 4, 1),
(25, 'Speed of work', 4, 0.8),
(26, 'Graphic design quality', 4, 1),
(27, 'User support', 4, 0.4),
(28, 'Attempt for individual improvement and new knowledge', 5, 0.5),
(29, 'Keeps working hours', 5, 0.5),
(30, 'Knowledge of the function of their department', 5, 0.5),
(31, 'Knowledge of iED', 5, 0.5),
(32, 'Flexibility and adaptability to change', 5, 0.6),
(33, 'Taking responsibility', 5, 0.8),
(34, 'Problem Solving Capability', 5, 0.5),
(35, 'Ability to work under pressure', 5, 0.5),
(36, 'Collaboration/Communication with colleagues and/or external partners', 5, 0.5),
(37, 'Interested, excited and willing to work', 5, 0.5),
(38, 'Accepts advice and instructions', 5, 0.3),
(39, 'General organization', 5, 0.5),
(40, 'Ability to perform work with precision', 5, 0.5),
(41, 'Ability to perform work consistently', 5, 0.4),
(42, 'Knowledge of trends / hot topics / programming', 5, 0.5),
(43, 'Speed of work', 5, 0.5),
(44, 'Code writing quality', 5, 0.9),
(45, 'User support', 5, 1),
(46, 'Quality of work', 1, 0.733333333),
(47, 'Speed ​​of work', 1, 0.3),
(48, 'Attempt for individual improvement', 1, 0.533333333),
(49, 'Keeps the deadlines', 1, 0.283333333),
(50, 'Keeps work hours', 1, 0.083333333),
(51, 'Knowledge of the department', 1, 0.433333333),
(52, 'Knowledge of iED', 1, 0.333333333),
(53, 'Knowledge of iED deliverables', 1, 0.483333333),
(54, 'Flexibility and adaptability to change', 1, 0.15),
(55, 'Taking responsibility', 1, 0.25),
(56, 'Personal effort', 1, 0.516666667),
(57, 'Problem Solving Capability', 1, 0.45),
(58, 'Ability to work under pressure', 1, 0.583333333),
(59, 'Communication with colleagues', 1, 0.216666667),
(60, 'Interested, excited and willing to work', 1, 0.183333333),
(61, 'Accept advice and instructions', 1, 0.4),
(62, 'Willingness for education and new knowledge', 1, 0.4),
(63, 'Volunteering and Sensitivity', 1, 0.116666667),
(64, 'Collaboration with colleagues and external partners', 1, 0.733333333),
(65, 'Offer help and willingness to colleagues', 1, 0.216666667),
(66, 'Interpersonal relationships with colleagues', 1, 0.25),
(67, 'Courtesy / friendliness', 1, 0.466666667),
(68, 'Systematically organizing', 1, 0.3),
(69, 'Ability to work with precision', 1, 0.25),
(70, 'Ability to work consistently', 1, 0.116666667),
(71, 'IT knowledge', 1, 0.366666667),
(72, 'organization / management', 1, 0.416666667),
(73, 'Knowledge of trends / hot topics', 1, 0.3),
(74, 'Ability to manage time / work', 1, 0.133333333),
(75, 'Quality of work', 2, 0.315),
(76, 'Speed ​​of work execution', 2, 0.175),
(77, 'Attempt for individual improvement', 2, 0.256666667),
(78, 'Consequence of work', 2, 0.198333333),
(79, 'Keeping hours', 2, 0.046666667),
(80, 'Attempt to improve his / her work', 2, 0.046666667),
(81, 'Knowledge of the function of the department', 2, 0.548333333),
(82, 'Knowledge of iED', 2, 0.431666667),
(83, 'Knowledge of iED deliverables', 2, 0.443333333),
(84, 'Flexibility and adaptability to change', 2, 0.338333333),
(85, 'Taking responsibility', 2, 0.373333333),
(86, 'Personal effort', 2, 0.28),
(87, 'Problem Solving Capability', 2, 0.455),
(88, 'Ability to work under pressure', 2, 0.326666667),
(89, 'Contact with colleagues', 2, 0.233333333),
(90, 'Interesting, excited and willing to work', 2, 0.046666667),
(91, 'Accept advice and instructions', 2, 0.198333333),
(92, 'Willingness for education and new knowledge', 2, 0.058333333),
(93, 'Volunteering and Sensitivity', 2, 0.058333333),
(94, 'Collaboration with colleagues and external partners', 2, 0.455),
(95, 'Offer help, willingness and service to colleagues', 2, 0.221666667),
(96, 'Interpersonal relationships with colleagues', 2, 0.21),
(97, 'Courtesy / friendliness', 2, 0.303333333),
(98, 'Systematically organizing and operating the iED', 2, 0.163333333),
(99, 'Ability to perform work with precision', 2, 0.093333333),
(100, 'IT knowledge', 2, 0.058333333),
(101, 'Knowledge of organization / management', 2, 0.406),
(102, 'Knowledge of trends / hot topics', 2, 0.233333333),
(103, 'Ability to manage time / work', 2, 0.093333333),
(104, 'Θεωρείς ότι ο τρόπος διαχείρισης και οργάνωσης του τμήματος συμβάλλει στην επίτευξη των στόχων?', 2, 0.75),
(105, 'Θεωρείς ότι η κατανομή εργασιών και αρμοδιοτήτων είναι δίκαιη και καλύπτει τις ανάγκες του τμήματος?', 2, 0.75),
(106, 'Τα όποια προβλήματα / θέματα προκύπτουν εντός του τμήματος λύνονται άμεσα και εύκολα?', 2, 0.75),
(107, 'Θεωρείς ότι υπάρχει ίση αντιμετώπιση όλων των ατόμων από τον υπεύθυνο του τμήματος?', 2, 0.75),
(108, 'Quality of work', 6, 0.49),
(109, 'Speed ​​of work execution', 6, 0.392),
(110, 'Consequence of work', 6, 0.392),
(111, 'Keeping hours', 6, 0.098),
(112, 'Attempt to improve his / her work', 6, 0.294),
(113, 'Knowledge of the function of the department', 6, 0.294),
(114, 'Knowledge of iED', 6, 0.49),
(115, 'Flexibility and adaptability to change', 6, 0.098),
(116, 'Taking responsibility', 6, 0.196),
(117, 'Problem Solving Capability', 6, 0.49),
(118, 'Ability to work under pressure', 6, 0.098),
(119, 'Contact with colleagues', 6, 0.49),
(120, 'Interesting, excited and willing to work', 6, 0.49),
(121, 'Volunteering and Sensitivity', 6, 0.098),
(122, 'Collaboration with colleagues and external partners', 6, 0.49),
(123, 'Offer help, willingness and service to colleagues', 6, 0.196),
(124, 'Courtesy / friendliness', 6, 0.49),
(125, 'Systematically organizing and operating the iED', 6, 0.294),
(126, 'Βαθμολογήστε την καθαριότητα του χώρου', 6, 0.6),
(127, 'Βαθμολογήστε την  κράτηση εισητηρίων ως προς το κόστος τους', 6, 0.6),
(128, 'Βαθμολογήστε την αποτελεσματικότητα οργάνωσης εκδηλώσεων/συναντήσεων στο Ied', 6, 0.6),
(129, 'Βαθμολογήστε την οικονομική διαχείριση του Ταμείου', 6, 0.6),
(130, 'Βαθμολογήστε την εξυπηρέρηση και πληροφόρηση που παρέχεται σε τρίτους τηλεφωνικά και διαζώσης', 6, 0.6),
(131, 'Δημιουργήθηκαν προβλήματα στην επικοινωνία; ', 6, 0.6),
(132, 'Έγινε έλεγχος οικονομικών τον προηγούμενο μήνα;', 6, 0.6),
(133, 'Attempt for individual improvement', 7, 0.1862),
(134, 'Consequence of work', 7, 0.3724),
(135, 'Attempt to improve his / her work', 7, 0.2793),
(136, 'Knowledge of iED', 7, 0.4655),
(137, 'Flexibility and adaptability to change', 7, 0.0931),
(138, 'Taking responsibility', 7, 0.1862),
(139, 'Personal effort', 7, 0.1862),
(140, 'Problem Solving Capability', 7, 0.4655),
(141, 'Ability to work under pressure', 7, 0.0931),
(142, 'Contact with colleagues', 7, 0.4655),
(143, 'Interesting, excited and willing to work', 7, 0.4655),
(144, 'Accept advice and instructions', 7, 0.4655),
(145, 'Willingness for education and new knowledge', 7, 0.3724),
(146, 'Collaboration with colleagues and external partners', 7, 0.4655),
(147, 'Interpersonal relationships with colleagues', 7, 0.1862),
(148, 'Courtesy / friendliness', 7, 0.4655),
(149, 'Systematically organizing and operating the iED', 7, 0.2793),
(150, 'Knowledge of organization / management', 7, 0.4655),
(151, 'Knowledge of trends / hot topics', 7, 0.0931),
(152, 'Πως πιστεύετε ότι ήταν η οικονομική διαχείριση το προηγούμενο διάστημα;', 7, 0.9975),
(153, 'Βαθμολογήστε το συντονισμό του iED από το CEO', 7, 0.9975),
(154, 'Βαθμολογήστε τον τρόπο διοίκησης του προσωπικού από τον CEO', 7, 0.9975),
(155, 'Βαθμολογήστε την αποτελεσματικότητα της στρατηγικής ανάπτυξης του iED ', 7, 0.9975),
(156, 'Quality of work', 8, 0.315),
(157, 'Speed ​​of work execution', 8, 0),
(158, 'Attempt for individual improvement', 8, 0.14),
(159, 'Consequence of work', 8, 0.28),
(160, 'Keeping hours', 8, 0.245),
(161, 'Attempt to improve his / her work', 8, 0.315),
(162, 'Knowledge of the function of the department', 8, 0.42),
(163, 'Knowledge of iED', 8, 0.245),
(164, 'Knowledge of iED deliverables', 8, 0.175),
(165, 'Flexibility and adaptability to change', 8, 0.385),
(166, 'Taking responsibility', 8, 0.49),
(167, 'Personal effort', 8, 0.105),
(168, 'Problem Solving Capability', 8, 0.315),
(169, 'Ability to work under pressure', 8, 0.245),
(170, 'Contact with colleagues', 8, 0.385),
(171, 'Interesting, excited and willing to work', 8, 0.175),
(172, 'Accept advice and instructions', 8, 0.21),
(173, 'Willingness for education and new knowledge', 8, 0.28),
(174, 'Volunteering and Sensitivity', 8, 0),
(175, 'Collaboration with colleagues and external partners', 8, 0.455),
(176, 'Offer help, willingness and service to colleagues', 8, 0.105),
(177, 'Interpersonal relationships with colleagues', 8, 0.28),
(178, 'Courtesy / friendliness', 8, 0.245),
(179, 'Systematically organizing and operating the iED', 8, 0.105),
(180, 'Ability to perform work at a speed', 8, 0),
(181, 'Ability to perform work with precision', 8, 0.105),
(182, 'Ability to perform work consistently', 8, 0.105),
(183, 'IT knowledge', 8, 0.315),
(184, 'Knowledge of organization / management', 8, 0.07),
(185, 'Knowledge of trends / hot topics', 8, 0.49),
(186, 'Σε ποιο βαθμό πιστεύετε ότι ο Content Manager ανταποκρίνεται στους στόχους της στρατηγικής περιεχομένου', 8, 0.6),
(187, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Content Manager όσον αφορά τον προγραμματισμό άρθρων;', 8, 0.6),
(188, 'Πως αξιολογείτε τη βοήθεια που λαμβάνετε σε σχέση με τη βελτίωση της σχεδίασης της σελίδας του IED;', 8, 0.6),
(189, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Content Manager;', 8, 0.6),
(190, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Content Manager;', 8, 0.6),
(191, 'Quality of work', 9, 0.42),
(192, 'Attempt for individual improvement', 9, 0.245),
(193, 'Consequence of work', 9, 0.175),
(194, 'Keeping hours', 9, 0.14),
(195, 'Attempt to improve his / her work', 9, 0.42),
(196, 'Knowledge of the function of the department', 9, 0.28),
(197, 'Knowledge of iED', 9, 0.105),
(198, 'Knowledge of iED deliverables', 9, 0.105),
(199, 'Flexibility and adaptability to change', 9, 0.35),
(200, 'Taking responsibility', 9, 0.385),
(201, 'Personal effort', 9, 0.105),
(202, 'Problem Solving Capability', 9, 0.35),
(203, 'Ability to work under pressure', 9, 0.245),
(204, 'Contact with colleagues', 9, 0.245),
(205, 'Interesting, excited and willing to work', 9, 0.35),
(206, 'Accept advice and instructions', 9, 0.21),
(207, 'Willingness for education and new knowledge', 9, 0.455),
(208, 'Collaboration with colleagues and external partners', 9, 0.385),
(209, 'Offer help, willingness and service to colleagues', 9, 0.105),
(210, 'Interpersonal relationships with colleagues', 9, 0.28),
(211, 'Courtesy / friendliness', 9, 0.175),
(212, 'Systematically organizing and operating the iED', 9, 0.105),
(213, 'Ability to perform work with precision', 9, 0.21),
(214, 'Ability to perform work consistently', 9, 0.21),
(215, 'IT knowledge', 9, 0.455),
(216, 'Knowledge of organization / management', 9, 0.07),
(217, 'Knowledge of trends / hot topics', 9, 0.42),
(218, 'Σε ποιο βαθμό πιστεύετε ότι ο SEO Expert/Copywriter ανταποκρίνεται στους στόχους του online/digital marketing', 9, 0.428571429),
(219, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον SEO Expert/Copywriter;', 9, 0.428571429),
(220, 'Πως αξιολογείτε τη βοήθεια που λαμβάνετε σε σχέση με τη βελτίωση της σχεδίασης της σελίδας του IED;', 9, 0.428571429),
(221, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον SEO Expert/Copywriter;', 9, 0.428571429),
(222, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Content Manager;', 9, 0.428571429),
(223, 'Πως αξιολογείτε τη γνώση του SEO Expert/Copywriter σχετικά με τις τελευταίες εξελίσεις σε SEO τεχνικές', 9, 0.428571429),
(224, 'Πως αξιολογείτε τo copy writing του SEO Expert/Copywriter;', 9, 0.428571429),
(225, 'Quality of work', 10, 0.315),
(226, 'Speed ​​of work execution', 10, 0),
(227, 'Attempt for individual improvement', 10, 0.14),
(228, 'Consequence of work', 10, 0.28),
(229, 'Keeping hours', 10, 0.245),
(230, 'Attempt to improve his / her work', 10, 0.315),
(231, 'Knowledge of the function of the department', 10, 0.42),
(232, 'Knowledge of iED', 10, 0.245),
(233, 'Knowledge of iED deliverables', 10, 0.175),
(234, 'Flexibility and adaptability to change', 10, 0.385),
(235, 'Taking responsibility', 10, 0.49),
(236, 'Personal effort', 10, 0.105),
(237, 'Problem Solving Capability', 10, 0.315),
(238, 'Ability to work under pressure', 10, 0.245),
(239, 'Contact with colleagues', 10, 0.385),
(240, 'Interesting, excited and willing to work', 10, 0.175),
(241, 'Accept advice and instructions', 10, 0.21),
(242, 'Willingness for education and new knowledge', 10, 0.28),
(243, 'Volunteering and Sensitivity', 10, 0),
(244, 'Collaboration with colleagues and external partners', 10, 0.455),
(245, 'Offer help, willingness and service to colleagues', 10, 0.105),
(246, 'Interpersonal relationships with colleagues', 10, 0.28),
(247, 'Courtesy / friendliness', 10, 0.245),
(248, 'Systematically organizing and operating the iED', 10, 0.105),
(249, 'Ability to perform work at a speed', 10, 0),
(250, 'Ability to perform work with precision', 10, 0.105),
(251, 'Ability to perform work consistently', 10, 0.105),
(252, 'IT knowledge', 10, 0.315),
(253, 'Knowledge of organization / management', 10, 0.07),
(254, 'Knowledge of trends / hot topics', 10, 0.49),
(255, 'Σε ποιο βαθμό πιστεύετε ότι ο Sοcial Media Manager ανταποκρίνεται στους στόχους του;', 10, 0.75),
(256, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Sοcial Media Manager;', 10, 0.75),
(257, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Sοcial Media Manager;', 10, 0.75),
(258, 'Πως αξιολογείτε τη γνώση του Sοcial Media Manager σχετικά με τις τελευταίες εξελίσεις στα social media;', 10, 0.75),
(259, 'Quality of work', 11, 0.315),
(260, 'Speed ​​of work execution', 11, 0),
(261, 'Attempt for individual improvement', 11, 0.14),
(262, 'Consequence of work', 11, 0.28),
(263, 'Keeping hours', 11, 0.245),
(264, 'Attempt to improve his / her work', 11, 0.315),
(265, 'Knowledge of the function of the department', 11, 0.42),
(266, 'Knowledge of iED', 11, 0.245),
(267, 'Knowledge of iED deliverables', 11, 0.175),
(268, 'Flexibility and adaptability to change', 11, 0.385),
(269, 'Taking responsibility', 11, 0.49),
(270, 'Personal effort', 11, 0.105),
(271, 'Problem Solving Capability', 11, 0.315),
(272, 'Ability to work under pressure', 11, 0.245),
(273, 'Contact with colleagues', 11, 0.385),
(274, 'Interesting, excited and willing to work', 11, 0.175),
(275, 'Accept advice and instructions', 11, 0.21),
(276, 'Willingness for education and new knowledge', 11, 0.28),
(277, 'Volunteering and Sensitivity', 11, 0),
(278, 'Collaboration with colleagues and external partners', 11, 0.455),
(279, 'Offer help, willingness and service to colleagues', 11, 0.105),
(280, 'Interpersonal relationships with colleagues', 11, 0.28),
(281, 'Courtesy / friendliness', 11, 0.245),
(282, 'Systematically organizing and operating the iED', 11, 0.105),
(283, 'Ability to perform work at a speed', 11, 0),
(284, 'Ability to perform work with precision', 11, 0.105),
(285, 'Ability to perform work consistently', 11, 0.105),
(286, 'IT knowledge', 11, 0.315),
(287, 'Knowledge of organization / management', 11, 0.07),
(288, 'Knowledge of trends / hot topics', 11, 0.49),
(289, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager επιβλέπει σωστά το Τμήμα Marketing;', 11, 0.428571429),
(290, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Marketing & Innovation Manager;', 11, 0.428571429),
(291, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager υποστηρίζει τα υπόλοιπα μέλη του τμήματος;', 11, 0.428571429),
(292, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager βοηθά στη συνολική επίτευξη των στόχων του τμήματος;', 11, 0.428571429),
(293, 'Πόσο αποτελεσματική είναι η επικοινωνία με τον Marketing & Innovation Manager;', 11, 0.428571429),
(294, 'Πόσο βαθμολογείτε τη συνεργασία σας με το τμήμα Marketing', 11, 0.428571429),
(295, 'Πόσο αποτελεσματικά εργάζεται ο Marketing & Innovation Manager για την ανάπτυξη του τμήματος Business Development;', 11, 0.428571429),
(296, 'Quality of work', 12, 0.303333333),
(297, 'Speed ​​of work execution', 12, 0.186666667),
(298, 'Attempt for individual improvement', 12, 0.233333333),
(299, 'Consequence of work', 12, 0.233333333),
(300, 'Keeping hours', 12, 0.21),
(301, 'Attempt to improve his / her work', 12, 0.046666667),
(302, 'Knowledge of the function of the department', 12, 0.326666667),
(303, 'Knowledge of iED', 12, 0.466666667),
(304, 'Knowledge of iED deliverables', 12, 0.046666667),
(305, 'Flexibility and adaptability to change', 12, 0.256666667),
(306, 'Taking responsibility', 12, 0.21),
(307, 'Personal effort', 12, 0.093333333),
(308, 'Problem Solving Capability', 12, 0.256666667),
(309, 'Ability to work under pressure', 12, 0.233333333),
(310, 'Contact with colleagues', 12, 0.396666667),
(311, 'Interesting, excited and willing to work', 12, 0.373333333),
(312, 'Accept advice and instructions', 12, 0.28),
(313, 'Willingness for education and new knowledge', 12, 0.35),
(314, 'Volunteering and Sensitivity', 12, 0.233333333),
(315, 'Collaboration with colleagues and external partners', 12, 0.373333333),
(316, 'Offer help, willingness and service to colleagues', 12, 0.21),
(317, 'Interpersonal relationships with colleagues', 12, 0.093333333),
(318, 'Courtesy / friendliness', 12, 0.35),
(319, 'Systematically organizing and operating the iED', 12, 0.093333333),
(320, 'Ability to perform work at a speed', 12, 0.163333333),
(321, 'Ability to perform work with precision', 12, 0.233333333),
(322, 'Ability to perform work consistently', 12, 0.163333333),
(323, 'IT knowledge', 12, 0.093333333),
(324, 'Knowledge of organization / management', 12, 0.116666667),
(325, 'Knowledge of trends / hot topics', 12, 0.186666667),
(326, 'Attempt for better individual and team results', 12, 0.186666667),
(327, 'Πόσο πιστεύετε ότι αυξήθηκαν οι επικοινωνίες του IED με νέους εταίρους', 12, 0.75),
(328, 'Αποτελεσματική επικοινωνία με τους εταίρους ', 12, 0.75),
(329, 'Παρακολούθηση του χρονοδιαγράμματος δικτύωσης των calls', 12, 0.75),
(330, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Network Assistant Manager?', 12, 0.75),
(331, 'Quality of work', 13, 0.303333333),
(332, 'Speed ​​of work execution', 13, 0.186666667),
(333, 'Attempt for individual improvement', 13, 0.233333333),
(334, 'Consequence of work', 13, 0.233333333),
(335, 'Keeping hours', 13, 0.21),
(336, 'Attempt to improve his / her work', 13, 0.046666667),
(337, 'Knowledge of the function of the department', 13, 0.326666667),
(338, 'Knowledge of iED', 13, 0.466666667),
(339, 'Knowledge of iED deliverables', 13, 0.046666667),
(340, 'Flexibility and adaptability to change', 13, 0.256666667),
(341, 'Taking responsibility', 13, 0.21),
(342, 'Personal effort', 13, 0.093333333),
(343, 'Problem Solving Capability', 13, 0.256666667),
(344, 'Ability to work under pressure', 13, 0.233333333),
(345, 'Contact with colleagues', 13, 0.396666667),
(346, 'Interesting, excited and willing to work', 13, 0.373333333),
(347, 'Accept advice and instructions', 13, 0.28),
(348, 'Willingness for education and new knowledge', 13, 0.35),
(349, 'Volunteering and Sensitivity', 13, 0.233333333),
(350, 'Collaboration with colleagues and external partners', 13, 0.373333333),
(351, 'Offer help, willingness and service to colleagues', 13, 0.21),
(352, 'Interpersonal relationships with colleagues', 13, 0.093333333),
(353, 'Courtesy / friendliness', 13, 0.35),
(354, 'Systematically organizing and operating the iED', 13, 0.093333333),
(355, 'Ability to perform work at a speed', 13, 0.163333333),
(356, 'Ability to perform work with precision', 13, 0.233333333),
(357, 'Ability to perform work consistently', 13, 0.163333333),
(358, 'IT knowledge', 13, 0.093333333),
(359, 'Knowledge of organization / management', 13, 0.116666667),
(360, 'Knowledge of trends / hot topics', 13, 0.186666667),
(361, 'Attempt for better individual and team results', 13, 0.186666667),
(362, 'Τηρούνται τα χρονικά πλαίσια για τη συγγραφή των προτάσεων;', 13, 1),
(363, 'Έχει γραφεί ο προβλεπόμενος αριθμός προτάσεων;', 13, 1),
(364, 'Βαθμολογήστε την ποιότητα των προτάσεων που υποβλήθηκαν από το iED', 13, 1),
(365, 'Quality of work', 14, 0.256666667),
(366, 'Speed ​​of work execution', 14, 0.233333333),
(367, 'Attempt for individual improvement', 14, 0.21),
(368, 'Consequence of work', 14, 0.373333333),
(369, 'Keeping hours', 14, 0.21),
(370, 'Attempt to improve his / her work', 14, 0.14),
(371, 'Knowledge of the function of the department', 14, 0.396666667),
(372, 'Knowledge of iED', 14, 0.396666667),
(373, 'Knowledge of iED deliverables', 14, 0.186666667),
(374, 'Flexibility and adaptability to change', 14, 0.256666667),
(375, 'Taking responsibility', 14, 0.396666667),
(376, 'Personal effort', 14, 0.186666667),
(377, 'Problem Solving Capability', 14, 0.396666667),
(378, 'Ability to work under pressure', 14, 0.35),
(379, 'Contact with colleagues', 14, 0.326666667),
(380, 'Interesting, excited and willing to work', 14, 0.21),
(381, 'Accept advice and instructions', 14, 0.186666667),
(382, 'Willingness for education and new knowledge', 14, 0.233333333),
(383, 'Volunteering and Sensitivity', 14, 0.116666667),
(384, 'Collaboration with colleagues and external partners', 14, 0.233333333),
(385, 'Offer help, willingness and service to colleagues', 14, 0.256666667),
(386, 'Interpersonal relationships with colleagues', 14, 0.21),
(387, 'Courtesy / friendliness', 14, 0.21),
(388, 'Systematically organizing and operating the iED', 14, 0.21),
(389, 'Ability to perform work at a speed', 14, 0.14),
(390, 'Ability to perform work with precision', 14, 0.186666667),
(391, 'Ability to perform work consistently', 14, 0.116666667),
(392, 'IT knowledge', 14, 0.07),
(393, 'Knowledge of organization / management', 14, 0.21),
(394, 'Knowledge of trends / hot topics', 14, 0.093333333),
(395, 'Σε ποιο βαθμό πιστεύετε ότι ο Network Manager επιβλέπει σωστά τον Network Assistant?', 14, 0.6),
(396, 'Πόσο πιστεύετε ότι αυξήθηκαν οι επικοινωνίες του IED με νέους εταίρους', 14, 0.6),
(397, 'Αποτελεσματική επικοινωνία με τους εταίρους ', 14, 0.6),
(398, 'Παρακολούθηση του χρονοδιαγράμματος δικτύωσης των calls', 14, 0.6),
(399, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Network Manager?', 14, 0.6),
(400, 'Quality of work', 15, 0.303333333),
(401, 'Speed ​​of work execution', 15, 0.186666667),
(402, 'Attempt for individual improvement', 15, 0.233333333),
(403, 'Consequence of work', 15, 0.233333333),
(404, 'Keeping hours', 15, 0.21),
(405, 'Attempt to improve his / her work', 15, 0.046666667),
(406, 'Knowledge of the function of the department', 15, 0.326666667),
(407, 'Knowledge of iED', 15, 0.466666667),
(408, 'Knowledge of iED deliverables', 15, 0.046666667),
(409, 'Flexibility and adaptability to change', 15, 0.256666667),
(410, 'Taking responsibility', 15, 0.21),
(411, 'Personal effort', 15, 0.093333333),
(412, 'Problem Solving Capability', 15, 0.256666667),
(413, 'Ability to work under pressure', 15, 0.233333333),
(414, 'Contact with colleagues', 15, 0.396666667),
(415, 'Interesting, excited and willing to work', 15, 0.373333333),
(416, 'Accept advice and instructions', 15, 0.28),
(417, 'Willingness for education and new knowledge', 15, 0.35),
(418, 'Volunteering and Sensitivity', 15, 0.233333333),
(419, 'Collaboration with colleagues and external partners', 15, 0.373333333),
(420, 'Offer help, willingness and service to colleagues', 15, 0.21),
(421, 'Interpersonal relationships with colleagues', 15, 0.093333333),
(422, 'Courtesy / friendliness', 15, 0.35),
(423, 'Systematically organizing and operating the iED', 15, 0.093333333),
(424, 'Ability to perform work at a speed', 15, 0.163333333),
(425, 'Ability to perform work with precision', 15, 0.233333333),
(426, 'Ability to perform work consistently', 15, 0.163333333),
(427, 'IT knowledge', 15, 0.093333333),
(428, 'Knowledge of organization / management', 15, 0.116666667),
(429, 'Knowledge of trends / hot topics', 15, 0.186666667),
(430, 'Attempt for better individual and team results', 15, 0.186666667),
(431, 'Σε ποιο βαθμό πιστεύετε ότι ο SUB Planner παρακολουθεί αποτελεσματικά την υλοποίηση των χρονοδιαγραμμάτων των υποβολών;', 15, 0.428571429),
(432, 'Σε ποιο βαθμό πιστεύετε ότι τηρούνται τα χρονοδιαγράμματα που αφορούν την Υποβολή προτάσεων', 15, 0.428571429),
(433, 'Προτείνει ο Sub Planner Ιδέες για τη συγγραφή νέων consept notes;', 15, 0.428571429),
(434, 'Συντονίζει αποτελεσματικά o Sub Planner τη συγγραφή των προτάσεων;', 15, 0.428571429),
(435, 'Πιστεύετε ότι ο Sub Planner συσχετίζει αποτελεσματικά τα κατάλληλα παραδοτέα του iED με το call ', 15, 0.428571429),
(436, 'Πιστεύετε ότι ο Sub Planner συσχετίζει αποτελσματικά τα έργα του iED με τα νέα calls', 15, 0.428571429),
(437, 'Πόσο σωστά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Sub Planner;', 15, 0.428571429),
(438, 'Interpersonal relationships with colleagues', 16, 1),
(439, 'Taking responsibility', 16, 1),
(440, 'Accept advice and instructions', 16, 1),
(441, 'Knowledge of the function of their department', 16, 1),
(442, 'Knowledge of iED', 16, 1),
(443, 'Volunteering and Sensitivity', 16, 1),
(444, 'Interesting, excited and willing to work', 16, 1),
(445, 'Communication with colleagues', 16, 1),
(446, 'Problem solving', 16, 1),
(447, 'Work under pressure', 16, 1),
(448, 'Courtesy / friendliness', 16, 1),
(449, 'Flexibility and adaptability to change', 16, 1),
(450, 'Creativity', 16, 1),
(451, 'Organizational aspects', 16, 1),
(452, 'Quality of work', 16, 1),
(453, 'Cosistency', 16, 1),
(454, 'Speed', 16, 1),
(455, 'How much do you deserve to be the worker of the month?', 16, 1),
(456, 'How much you are willing to work?', 16, 1),
(457, 'How much are you trying to improve?', 16, 1),
(458, 'How much do you keep working hours?', 16, 1),
(459, 'Willingness for education and new knowledge', 16, 1),
(460, 'Offer help, willingness and service to colleagues', 16, 1),
(461, 'Personal effort', 16, 1),
(462, 'Percent of the person\'s working hours logged in Teamwork:\r\n', 17, 1),
(463, 'Άρτια ολοκλήρωση των έργων, σε ποσοστό απορρόφησης του budget\r\n', 18, 1),
(464, 'Μείωση των travel expenses του 2018, συγκριτικά με το 2017\r\n', 18, 1),
(465, 'Μείωση του συνολικού κόστους υλοποίησης ανά έργο, συγκριτικά με το 2017\r\n', 18, 1),
(466, 'Ολοκλήρωση Συστήματος Αξιολόγησης\r\n', 20, 1),
(467, 'Ολοκλήρωση συστήματος Οικονομικής διαχείρισης\r\n', 20, 1),
(468, 'Ολοκλήρωση του οργανογράμματος\r\n', 20, 1),
(469, 'Διαχείριση προσωπικού\r\n', 20, 1),
(470, 'Αποτελεσματική Οικονομική διαχείριση\r\n', 20, 1),
(471, 'Υλοποίηση Στρατηγικής του Ied\r\n\r\n', 20, 1),
(472, 'Αποτελεσματική Διαχείριση του IED\r\n', 20, 1),
(473, 'Increase traffic/followers in social media \r\n', 21, 1),
(474, 'Increase traffic/visits in webpage\r\n', 21, 1),
(475, 'Increase subscibers in website\r\n', 21, 1),
(476, 'Create specific content for promotion\r\n', 21, 1),
(477, 'Προώθηση των custom services σε EBSO MED\r\n', 21, 1),
(478, 'Προώθηση της καινούριας THEMIDA ως προϊόν\r\n\r\n', 21, 1),
(479, 'Επαναπροώθηση του Valuater\r\n', 21, 1),
(480, 'Προώθηση του AXIOL\r\n\r\n', 21, 1),
(481, 'ΑΡΙΘΜΟΣ ΥΠΟΒΟΛΩΝ ERASMUS ΓΙΑ ΤΟ 2018\r\n', 22, 1),
(482, 'Συγγραφή δικών μας προτάσεων. Υποβολές iED\r\n', 22, 1),
(483, 'Συμμετοχές σε Η2020\r\n', 22, 1),
(484, 'Η2020 που γράφουμε\r\n', 22, 1),
(485, 'Αριθμός calls στα οποία στοχεύουμε\r\n', 22, 1),
(486, 'Αριθμός νέων ιδεών που ετοιμάζουμε για concept notes\r\n', 22, 1),
(487, 'Υπογραφή μνημονίων συνεργασίας\r\n', 22, 1),
(488, 'H2020 ΙΔΙΟΣ ΜΕ Σ1\r\n', 22, 1),
(489, 'REC\r\n', 22, 1),
(490, 'LIFE\r\n', 22, 1),
(491, 'EUROPEAID\r\n', 22, 1),
(492, 'AΛΛΑ ΠΡΟΓΡΑΜΜΑΤΑ\r\n', 22, 1),
(493, 'Να δούμε άλλες πηγές π.χ. Coca cola fund\r\n', 22, 1),
(494, 'Ανάπτυξη της δυνατότητας συγγραφής προτάσεων. Αναζήτηση εξωτερικών συνεργατών\r\n', 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `ColliguesResult` double NOT NULL,
  `SelfEvalResult` double NOT NULL,
  `TeamworkResult` double NOT NULL,
  `GoalsPD` double NOT NULL,
  `GoalsPP` double NOT NULL,
  `Total` double NOT NULL,
  `Period` int(11) NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`ColliguesResult`, `SelfEvalResult`, `TeamworkResult`, `GoalsPD`, `GoalsPP`, `Total`, `Period`, `UserId`) VALUES
(80, 79, 0, 0, 0, 21.52, 1, 1),
(80, 80, 0, 0, 0, 21.6, 1, 2),
(80, 80, 0, 0, 0, 21.6, 1, 5),
(78, 80, 0, 0, 0, 21.22, 1, 7),
(76.5, 80, 90, 81.1, 82.19512195122, 82.661829268293, 1, 8),
(81, 80, 91, 81.1, 80.204081632653, 63.497979591837, 1, 9),
(75, 80, 70, 104, 116, 76.17, 1, 10),
(80, 79, 77, 104, 105, 91.2, 1, 11),
(79, 78, 74, 80, 63, 74.13, 2, 1),
(80, 79, 80, 80, 63, 75.84, 2, 2),
(80, 80, 90, 80, 73, 80.77, 2, 3),
(81, 80, 70, 2088.6, 2107.8048780488, 1066.6131707317, 2, 8),
(80, 80, 90, 2088.6, 2071.0204081633, 1062.3948979592, 2, 9),
(79, 78, 74, 80, 63, 74.13, 3, 1),
(80, 79, 80, 80, 63, 75.84, 3, 2),
(80, 80, 90, 80, 73, 80.77, 3, 3),
(81, 80, 70, 77.433333333333, 76.991869918699, 76.426382113821, 3, 8),
(80, 80, 90, 77.433333333333, 77.755102040816, 81.219557823129, 3, 9),
(81, 80, 90, 457, 548, 289.16, 3, 13),
(79, 80, 80, 457, 471, 267.9, 3, 17),
(40, 80, 80, 24502582, 32502792, 13926346.78, 3, 18);

-- --------------------------------------------------------

--
-- Table structure for table `userofdepartment`
--

CREATE TABLE `userofdepartment` (
  `userID` int(11) NOT NULL,
  `DeptID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userofdepartment`
--

INSERT INTO `userofdepartment` (`userID`, `DeptID`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 3),
(9, 3),
(10, 5),
(11, 5),
(12, 5),
(13, 4),
(14, 4),
(15, 4),
(16, 4),
(17, 4),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 3),
(25, 3),
(26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userposition`
--

CREATE TABLE `userposition` (
  `userID` int(11) NOT NULL,
  `positionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`userID`,`QuestID`,`target`,`period`),
  ADD KEY `fk7` (`QuestID`),
  ADD KEY `period` (`period`);

--
-- Indexes for table `credentials`
--
ALTER TABLE `credentials`
  ADD PRIMARY KEY (`username`,`password1`),
  ADD KEY `fk_5` (`userID`);

--
-- Indexes for table `ieddepartment`
--
ALTER TABLE `ieddepartment`
  ADD PRIMARY KEY (`DeptID`);

--
-- Indexes for table `ieduser`
--
ALTER TABLE `ieduser`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `rank1` (`rank1`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
  ADD PRIMARY KEY (`PeriodID`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`positionID`);

--
-- Indexes for table `questionares`
--
ALTER TABLE `questionares`
  ADD PRIMARY KEY (`QID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`QuestID`),
  ADD KEY `fk6` (`QID`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`Period`,`UserId`);

--
-- Indexes for table `userofdepartment`
--
ALTER TABLE `userofdepartment`
  ADD PRIMARY KEY (`userID`,`DeptID`),
  ADD KEY `fk_2_dept` (`DeptID`);

--
-- Indexes for table `userposition`
--
ALTER TABLE `userposition`
  ADD PRIMARY KEY (`userID`,`positionID`),
  ADD KEY `fk_4` (`positionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ieddepartment`
--
ALTER TABLE `ieddepartment`
  MODIFY `DeptID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ieduser`
--
ALTER TABLE `ieduser`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
  MODIFY `PeriodID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `positionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `questionares`
--
ALTER TABLE `questionares`
  MODIFY `QID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `QuestID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=495;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `fk7` FOREIGN KEY (`QuestID`) REFERENCES `questions` (`QuestID`),
  ADD CONSTRAINT `fk8` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`),
  ADD CONSTRAINT `fk9` FOREIGN KEY (`period`) REFERENCES `periods` (`PeriodID`);

--
-- Constraints for table `credentials`
--
ALTER TABLE `credentials`
  ADD CONSTRAINT `fk_5` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `fk6` FOREIGN KEY (`QID`) REFERENCES `questionares` (`QID`);

--
-- Constraints for table `userofdepartment`
--
ALTER TABLE `userofdepartment`
  ADD CONSTRAINT `fk_1_user` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`),
  ADD CONSTRAINT `fk_2_dept` FOREIGN KEY (`DeptID`) REFERENCES `ieddepartment` (`DeptID`);

--
-- Constraints for table `userposition`
--
ALTER TABLE `userposition`
  ADD CONSTRAINT `fk_3` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`),
  ADD CONSTRAINT `fk_4` FOREIGN KEY (`positionID`) REFERENCES `positions` (`positionID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
