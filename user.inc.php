<?php
  // This is where most of the magic is taking place. Base class for the user, getters, setters (uh boring :-/).

class User extends dbh{

  private $userid;
  private $firstname;
  private $lastname;
  private $position;
  private $department;
  private $position_text;
  private $periodTitle;

  function __construct($userid, $firstname, $lastname, $position){
    $this->userid = $userid;
    $this->firstname = $firstname;
    $this->lastname = $lastname;
    $this->position = $position;
    $this->position_text = $this->getUserPosition($position)[0][0];
    if(isset($_SESSION['Period'])){
      $period = $_SESSION['Period'];
  }else{
      $period = 6;
  }
  $this->periodTitle = $this->getPeriodName($period);
}

public function __toString()
{
    return $this->getLastname();
}

public function getUserid()
{
    return $this->userid;
}

public function setUserid($userid)
{
    $this->userid = $userid;

    return $this;
}

public function getFirstname()
{
    return $this->firstname;
}

public function setFirstname($firstname)
{
    $this->firstname = $firstname;

    return $this;
}

public function getLastname()
{
    return $this->lastname;
}

public function setLastname($lastname)
{
    $this->lastname = $lastname;

    return $this;
}

public function setPosition($position)
{
    $this->position = $position;

    return $this;
}

public function getPosition()
{
    return $this->position;
}

public function getPositionText(){
    return $this->position_text;
}

public function setPeriod($periodTitle){
    $this->periodTitle =$periodTitle;
}

public function getPeriod(){
    return str_replace(' ', '', $this->periodTitle);
}
    // Authentication routine. Get all users from the database, check given creds, return -1 if none found,
    // return userid if they are correct. TBH i can work more. I dont like the way its written, nor the logic.
    // I will change its logic so none of the database drawn items can interact with  the page.

public function getPeriodName($periodId){
    $stmt = $this->connect()->query("Select participants from periods where periodId = ".$periodId);
    $result = $stmt->fetch();
    $this->setPeriod($result[0]);
    return $result[0];
}



public function authenticate($username,$password){
    $ret = -1;
    $stmt = $this->connect()->query('select * from credentials where username like "'.$username.'"');
    $result = $stmt->fetch();
    if(sizeof($result)>0){
      echo password_hash($password, PASSWORD_DEFAULT);
      echo '<br>';
      $passwordHash = $result[1];
      echo $passwordHash;
      if(password_verify($password, $passwordHash)){
        return $result[2];
    }else{
        return -1;
    }
}else{
  return -1;
}
}

    // QUERY -> gets the department of the user.
public function getUserDepartment(){
    $stmt = $this->connect()->query('select deptid from userofdepartment where userid = '.$this->getUserid());
    $result = $stmt->fetch();
    return $result[0];
}

    // QUERY -> gets the colligues of the user.
public function getUserColligues(){
    $per = $this->getPeriod();
    $stmt = $this->connect()->query("select * from ".$per." where userID in ( select userID from userofdepartment where DeptID = ".$this->getUserDepartment().")");
    $result = $stmt->fetchAll();

    $colligues = array();
    for ($i = 0; $i < sizeof($result); $i++) {

      $newPerson = new User($result[$i][0],$result[$i][1],$result[$i][2],$result[$i][3]);

      array_push($colligues,$newPerson);

  }

  $notTasos2nd = array(3,8,11,13,14,18,19,12,10);
  if(!in_array($this->getUserId(),$notTasos2nd)){
      $stmt1 = $this->connect()->query('Select * from '.$per.' where lastname like "Vasileiadis"');
      $result = $stmt1->fetch();
      if($stmt1->rowCount()){
        $newPerson = new User($result[0],$result[1],$result[2],$result[3]);
        array_push($colligues,$newPerson);
    }
}

return $colligues;
}

    // QUERY -> gets the title each user has.
public function getUserPosition($userrank){
    $stmt = $this->connect()->query("SELECT Title FROM positions WHERE positionID = ".$userrank);
    $result = $stmt->fetchAll();

    return $result;
}


    //Query -> It gets the questions. -> can be modified to work with $this
public function getSingleQuestions($person){
    $title = $person->getUserPosition($person->getPosition());
    $stmt = $this->connect()->query("select * from questions where qid in(
      select qid from questionares where QSubtype = '".$title[0][0]."')");
    $questions = $stmt->fetchAll();


    return $questions;
}

public function getIT(){
    $stmt1 = $this->connect()->query("select * from questions where qid = 3");
    $ret = $stmt1->fetchAll();
    return $ret;
}

public function getQids(){
    $title = $this->getUserPosition($this->getPosition());
    $stmt = $this->connect()->query("select qid from questionares where QSubtype = '".$title[0][0]."'");
    $ret = $stmt->fetch();
    return $ret[0];
}

    //Query -> gets the questions regarding the second tier questions
public function getSelfEval(){
    $stmt = $this->connect()->query("select * from questions where qid = 16");
    $questions = $stmt->fetchAll();
    return $questions;
}


public function getUserData($userid){
    $stmt = $this->connect()->query('Select * from ieduser where userid = '.$userid);
    $result = $stmt->fetch();
    return $result;
}
    //Here gets real. I use this method to get all colligues the user has to answer for.
    // long story short, i check for ceo and supervisors, drawing the according questions.
public function getPeople(){
    if(strpos($this->getUserPosition($this->getPosition())[0][0], 'Supervisor') !== false){
      $stmt = $this->connect()->query("select * from ".$this->getPeriod()." where rank1 in (select positionID from positions where Title like '%Supervisor%') and lastname not like '".$this->getLastname()."'");
      $result = $stmt->fetchAll();



      $colligues = array();
      for ($i = 0; $i < sizeof($result); $i++) {

        $newPerson = new User($result[$i][0],$result[$i][1],$result[$i][2],$result[$i][3]);

        array_push($colligues,$newPerson);

    }
    $temp = $this->getUserColligues();
    foreach($temp as $p){
        array_push($colligues,$p);
    }

    if($this->getUserId() == 19 ){
        $kp = 0;
        foreach($colligues as $c){
          if($c->getLastname() == "Lagos"){
            unset($colligues[$kp]);
            break;
        }
        $kp++;
    }
}elseif($this->getUserid() == 20){
    $kp = 0;
    foreach($colligues as $c){
      if($c->getLastname() == "Petsas"){
        unset($colligues[$kp]);
        break;
    }
    $kp++;
}}
$ret = array();
foreach($colligues as $r){
  array_push($ret,$r);
}
return $ret;
}else{
    $colligues = $this->getUserColligues();
    return $colligues;
}

}

public function getTextDepartment(){
  $stmt = $this->connect()->query('Select Title from ieddepartment where DeptID = '.$this->getUserDepartment());
  $result = $stmt->fetch();
  return $result[0];
}

public function getTeamwork(){
  $stmt =  $this->connect()->query('Select * from questions where QID = 17');
  $result = $stmt->fetchAll();
  return $result;
}

public function isSupervisor(){
  $os = array(2,4,6,10,13,14);
  if(in_array($this->getPosition(), $os)){
    return true;
}else{
    return false;
}
}

public function getDepartmentGoals(){
  $deptId = $this->getUserDepartment();
  switch($deptId){
    case 1:
    $goals = 18;
    break;
    case 2:
    $goals = 22;
    break;
    case 3:
    $goals = 19;
    break;
    case 4:
    $goals = 21;
    break;
    case 5:
    $goals = 20;
    break;
}
$stmt = $this->connect()->query('SELECT * from questions where QID = '.$goals);
$result = $stmt->fetchAll();
return $result;
}

public function insertData($userID,$QuestID,$target,$period,$Ans1,$res){
  $dept = "'".$this->getDeptTitle()."'";
  if($target == 'dept'){
     $sql = "INSERT INTO answers (userID, QuestID, target, period,Ans1,Result)
     VALUES ($userID, $QuestID,$dept,$period,$Ans1,$res)";
 }elseif($target == 'IT'){
  $sql = "INSERT INTO answers (userID, QuestID, target, period,Ans1,Result)
  VALUES ($userID, $QuestID,'".$target."',$period,$Ans1,$res)";
  echo $sql;
}else{
  $stmt = $this->connect()->query("Select userID from ieduser where lastname like '".$target."'");
  $result = $stmt->fetch();
  $sql = "INSERT INTO answers (userID, QuestID, target, period,Ans1,Result)
  VALUES ($userID, $QuestID, $result[0],$period,$Ans1,$res)";
}

$this->connect()->exec($sql);

}

public function UpdateMults($start,$finish,$toEnter){
    $index = 0;
    for($i = $start;$i<=$finish;$i++){
      $stmt = $this->connect()->query('UPDATE questions SET Multiplier = '.$toEnter[$index].' WHERE QuestID ='.$i);

  }

}

public function getMult($questid){
    $stmt = $this->connect()->query('select Multiplier from questions where QuestID = '.$questid);
    $result = $stmt->fetch();
    echo $result[0];
    return $result[0];
}

public function checkAnswered($period){
    $stmt = $this->connect()->query("select count(DISTINCT target) from answers where userid = ".$this->getUserid()." and period = ".$period);
    $result = $stmt->fetchAll();

    if(sizeof($result)>5){
      return true;
  }else{
      return false;
  }
}

public function getImplementationGoalResults($period){
    $stmt=$this->connect()->query('SELECT * FROM `answers` WHERE period = '.$period.' and questid = 463 or questid = 464 or questid = 465');
    $result = $stmt->fetchAll();
    $answers = array();
    foreach($result as $r){
      array_push($answers,$r[4]);
  }

  $lpmResPerc = array(50,10,50);
  $pmResPerc = array(50,90,50);

  $setGoal = array(100,-10,-10);

  $GoalSignificance = array(60,20,20);


  $lpmGoalResult = array((($lpmResPerc[0]*$answers[0])/$setGoal[0])*100,(($lpmResPerc[1]*$answers[1])/$setGoal[1])*100,(($lpmResPerc[2]*$answers[2])/$setGoal[2])*100);
  $LPMFinalResults = array_sum($lpmGoalResult)/array_sum($lpmResPerc);

  $pmGoalResult = array((($pmResPerc[0]*$answers[0])/$setGoal[0])*100,(($pmResPerc[1]*$answers[1])/$setGoal[1])*100,(($pmResPerc[2]*$answers[2])/$setGoal[2])*100);
  $PMFinalResults = array_sum($pmGoalResult)/array_sum($pmResPerc);

  $DeptGoalResult = array((($GoalSignificance[0]*$answers[0])/$setGoal[0])*100,(($GoalSignificance[1]*$answers[1])/$setGoal[1])*100,(($GoalSignificance[2]*$answers[2])/$setGoal[2])*100);
  $DeptFinalResult = array_sum($DeptGoalResult)/array_sum($GoalSignificance);


  $lpm = round($LPMFinalResults, 2);
  $pm = round($PMFinalResults, 2);
  $dpt = round($DeptFinalResult, 2);

  $Impl = array($lpm,$pm,$dpt);
  return $Impl;
}

public function getSubmissionGoalResults($period){
    $stmt=$this->connect()->query("SELECT * FROM `answers` WHERE period = ".$period." and Questid between 481 and 494 ORDER BY `answers`.`QuestID` ASC");
    $result = $stmt->fetchAll();
    $answers = array();


    foreach($result as $r){
      array_push($answers,$r[4]);
  }

  $FNMResPerc = array(35,5,40,10,20,0,0,0,40,40,40,40,40,10);
  $FNAResPerc = array(20,5,30,10,10,0,0,0,30,30,30,30,30,10);
  $SPResPerc = array(35,30,15,30,50,80,0,0,15,15,15,15,15,40);
  $PWResPerc = array(10,60,15,50,20,20,0,0,15,15,15,15,15,40);


  $setGoal = array(16,1,0.0001,0.0001,5,7,0.0001,0.0001,0.0001,0.0001,1,4,1,1);

  $GoalSignificance = array(23,20,15,10,10,8,0,0,2,2,3,3,2,2);

  $FNMGoalResult = array();

  for($i = 0;$i<sizeof($answers);$i++){
      array_push($FNMGoalResult,(($FNMResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $FNMFinalResults = (array_sum($FNMGoalResult)/array_sum($FNMResPerc));

  $FNAGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($FNAGoalResult,(($FNAResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $FNAFinalResults = array_sum($FNAGoalResult)/array_sum($FNAResPerc);

  $SPGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($SPGoalResult,(($SPResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $SPFinalResults = array_sum($SPGoalResult)/array_sum($SPResPerc);

  $PWGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($PWGoalResult,(($PWResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $PWFinalResults = array_sum($PWGoalResult)/array_sum($PWResPerc);

  $DeptGoalResults = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($DeptGoalResults,(($GoalSignificance[$i]*$answers[$i])/$setGoal[$i]));
  }
  $DeptFinalResult = array_sum($DeptGoalResults)/array_sum($GoalSignificance);

  $fmn = round($FNMFinalResults, 2);
  $fna = round($FNAFinalResults, 2);
  $sp = round($SPFinalResults, 2);
  $pw = round($PWFinalResults, 2);
  $dpt = round($DeptFinalResult, 2);

  $Subm = array($fmn,$fna,$sp,$pw,$dpt);
  return $Subm;
}

public function getManagmentGoalResults($period){
    $stmt=$this->connect()->query("SELECT * FROM `answers` WHERE period = ".$period." and Questid between 466 and 472 ORDER BY `answers`.`QuestID` ASC");
    $result = $stmt->fetchAll();
    $answers = array();


    foreach($result as $r){
      array_push($answers,$r[4]);
  }

  $CEOResPerc = array(100,50,100,30,50,80,20);
  $SecrResPerc = array(0,50,0,70,50,20,80);

  $setGoal = array(100,30,100,100,100,100,100);

  $GoalSignificance = array(10,10,10,30,20,0,20);

  $CEOGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($CEOGoalResult,(($CEOResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $FNMFinalResults = (array_sum($CEOGoalResult)/array_sum($CEOResPerc));

  $SecrGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($SecrGoalResult,(($SecrResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $SecrFinalResults = array_sum($SecrGoalResult)/array_sum($SecrResPerc);

  $DeptGoalResults = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($DeptGoalResults,(($GoalSignificance[$i]*$answers[$i])/$setGoal[$i]));
  }
  $DeptFinalResult = array_sum($DeptGoalResults)/array_sum($GoalSignificance);

  $ceo =  round($FNMFinalResults, 2);
  $secr =  round($SecrFinalResults, 2);
  $dpt =  round($DeptFinalResult, 2);

  $mngm = array($ceo,$secr,$dpt);
  return $mngm;
}

	//it
public function getITGoalResults($period){
    $stmt=$this->connect()->query("SELECT * FROM `answers` WHERE period = ".$period." and  Questid between 1 and 9 ");
    $result = $stmt->fetchAll();
    $answers1 = array();


    foreach($result as $r){
      array_push($answers1,$r[4]);
  }

  $answers2 = array();

  for($ptr = 0;$ptr<9;$ptr++){
      $sum = 0;
      for($i = 0;$i<sizeof($answers1);$i=$i+9){
        echo " ".$ptr." ".$i." ".($ptr + $i)."/";
        $sum+=$answers1[$ptr + $i];
    }
    array_push($answers2,$sum);
}

$answers = array();
foreach($answers2 as $ans){
  array_push($answers,($ans/(sizeof($answers1)/9)));
}

echo "<pre>";
print_r($answers);
print_r($answers2);
      // /9 -> POSA KOMMATIA EXEIS. PEREPEI NA PROSTETHOUN , NA PAN -/ KAI NA MPOUN SE ENA PINAKA 9ARI

$DesResPerc = array(70,70,30,30,60,60,60,50,60);
$DevResPerc = array(30,30,70,70,40,40,40,50,40);

$setGoal = array(100,100,100,100,100,100,100,100,100);

$GoalSignificance = array(11,11,11,11,12,11,11,11,11);

$DesGoalResult = array();

for($i = 0;$i<sizeof($answers);$i++){
  array_push($DesGoalResult,(($DesResPerc[$i]*$answers[$i])/$setGoal[$i]));
}
$DesFinalResults = (array_sum($DesGoalResult)/array_sum($DesResPerc));

$DevGoalResult = array();
for($i = 0;$i<sizeof($answers);$i++){
  array_push($DevGoalResult,(($DevResPerc[$i]*$answers[$i])/$setGoal[$i]));
}
$DevFinalResults = array_sum($DevGoalResult)/array_sum($DevResPerc);

$DeptGoalResults = array();
for($i = 0;$i<sizeof($answers);$i++){
  array_push($DeptGoalResults,(($GoalSignificance[$i]*$answers[$i])/$setGoal[$i]));
}
$DeptFinalResult = array_sum($DeptGoalResults)/array_sum($GoalSignificance);


$Dev = round($DevFinalResults, 2);
$Des = round($DesFinalResults, 2);
$dpt = round($DeptFinalResult, 2);
$Dev = $DevFinalResults;
$Des = $DesFinalResults;
$dpt = $DeptFinalResult;


$it = array($Dev,$Des,$dpt);
return $it;
}
	//it

public function getMarketingGoalResults($period){
    $stmt=$this->connect()->query("SELECT * FROM `answers` WHERE period = ".$period." and Questid between 473 and 480 ORDER BY `answers`.`QuestID` ASC");
    $result = $stmt->fetchAll();
    $answers = array();
    foreach($result as $r){
      array_push($answers,$r[4]);
  }

  $BDMResPerc = array(25,25,50,10,0,0,0,0);
  $CMResPerc = array(50,15,20,30,0,0,0,0);
  $SCResPerc = array(10,20,25,60,0,0,0,0);
  $JSMResPerc = array(15,10,5,0,0,0,0,0);


  $setGoal = array(15,15,15,288,0.001,0.001,0.001,0.001);

  $GoalSignificance = array(25,25,25,25,0,0,0,0);

  $BDMGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($BDMGoalResult,(($BDMResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $BDMFinalResults = (array_sum($BDMGoalResult)/array_sum($BDMResPerc));

  $CMGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($CMGoalResult,(($CMResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $CMGFinalResults = array_sum($CMGoalResult)/array_sum($CMResPerc);

  $SCGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($SCGoalResult,(($SCResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $SCFinalResults = array_sum($SCGoalResult)/array_sum($SCResPerc);

  $JSMGoalResult = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($JSMGoalResult,(($JSMResPerc[$i]*$answers[$i])/$setGoal[$i]));
  }
  $JSMFinalResults = array_sum($JSMGoalResult)/array_sum($JSMResPerc);

  $DeptGoalResults = array();
  for($i = 0;$i<sizeof($answers);$i++){
      array_push($DeptGoalResults,(($GoalSignificance[$i]*$answers[$i])/$setGoal[$i]));
  }
  $DeptFinalResult = array_sum($DeptGoalResults)/array_sum($GoalSignificance);

  $bd = round($BDMFinalResults, 2);
  $cmg = round($CMGFinalResults, 2);
  $sc = round($SCFinalResults, 2);
  $jsm = round($JSMFinalResults, 2);
  $dpt = round($DeptFinalResult, 2);

  $markt = array($bd,$cmg,$sc,$jsm,$dpt);
  return $markt;
}



public function getSelfEvaluationResults($period){
    $stmt = $this->connect()->query("SELECT Result FROM `answers` WHERE QuestID in (SELECT questid FROM `questions` WHERE qid = 16) and userID = ".$this->getUserid()." and period = ".$period);
    $result = $stmt->fetchAll();
    $total = 0;
    foreach($result as $r){
      $total +=$r['Result'];
  }
  $max = sizeof($result) * 10;
  $finalRsult = ($total / $max)*100;
  return round($finalRsult, 0);
}

public function getTeamworkResults($period){
    $stmt = $this->connect()->query("select result from answers where questid = 462 and period = ".$period." and target = ".$this->getUserid());
    $result = $stmt->fetch();

    return $result[0];
}

public function getColligueEval($period){
    $stmt = $this->connect()->query("SELECT sum(Result),count(distinct userid) FROM `answers` WHERE target = ".$this->getUserID()." and period = ".$period." and QuestID in (select QuestID from questions where qid = ".$this->getQids().")");
    $result = $stmt->fetchAll();

    $res = $result[0][0]/$result[0][1];

    return round($res,0);
}

public function checkINPeriod($userID){
    $stmt = $this->connect()->query("select userid from ".str_replace(' ', '', $this->getPeriod()));


    $result = $stmt->fetchAll();
    foreach($result as $r){
      if($r[0] == $userID){
        return false;
    }
}
return true;
}

public function checkAllAnswerd($period){
    $stmt = $this->connect()->query("SELECT count(DISTINCT userid) FROM `answers` where period = ".$period);
    $stmt2 = $this->connect()->query("SELECT count(DISTINCT userid) FROM ".$this->getPeriod());

    $result1 = $stmt->fetch();
    $result2 = $stmt2->fetch();
    if($result1[0]==$result2[0]){
      return true;
  }
  return false;
}

public function checkDeptAnswered($period){
    $stmt = $this->connect()->query("select * from answers where target like '".$this->getDeptTitle()."' and period = ".$period);
    $res = $stmt->fetchAll();

    if($stmt->rowCount() != 0){
       return false;
   }
   return true;
}

public function getDeptTitle(){
  $stmt = $this->connect()->query("select Title from ieddepartment where DeptID in (select DeptID from userofdepartment where userId = ".$this->getUserid().")");
  $result = $stmt->fetch();
  return $result[0];
}

public function getPeriodData(){
  $stmt = $this->connect()->query("Select * from periods");
  $result = $stmt->fetchAll();

  return $result;
}

public function getParticipants($id){
  $stmt1 = $this->connect()->query("select participants from periods where periodid = ".$id);
  $name = $stmt1->fetch();
 // echo $name[0];
  $stmt = $this->connect()->query("Select * from ".$name[0]);
  $result = $stmt->fetchAll();
  $parts = array();

  foreach($result as $r){
    $user = new User($r[0],$r[1],$r[2],$r[3]);
    $person = array($user,$r[4]);
    array_push($parts,$person);
}

return $parts;

}

public function getPeriodId($perTitle){
  $stmt = $this->connect()->query("select periodId from periods where Title = '".$perTitle."'");
  $result = $stmt->fetch();
  return $result[0];
}

public function getDeptResults($period){
  switch($this->getUserID()){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 26:
    return $this->getImplementationGoalResults($period);
    break;
    case 8:
    case 9:
    case 25:
    return $this->getITGoalResults($period);
    break;
    case 10:
    case 11:
    case 12:
    return $this->getManagmentGoalResults($period);
    break;
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    return $this->getMarketingGoalResults($period);
    break;
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    return $this->getSubmissionGoalResults($period);
    break;
    default:
    return '0%';
}
}

public function getNumber($numbers){
  switch($this->getPosition()){
    case 1:
    return $numbers[1];
    break;
    case 2:
    return $numbers[0];
    break;
    case 3:
    return $numbers[1];
    break;
    case 4:
    return $numbers[0];
    break;
    case 5:
    return $numbers[1];
    break;
    case 6:
    return $numbers[0];
    break;
    case 7:
    return $numbers[1];
    break;
    case 8:
    return $numbers[2];
    break;
    case 9:
    return $numbers[3];
    break;
    case 10:
    return $numbers[0];
    break;
    case 11:
    return $numbers[1];
    break;
    case 12:
    return $numbers[3];
    break;
    case 13:
    return $numbers[0];
    break;
    case 14:
    return $numbers[2];
    break;
    case 15:
    return 0;
    break;
}
}

public function InsertResults($one,$two,$three,$four,$five,$six,$period,$id){
  $sql = "INSERT INTO `results` (`ColliguesResult`, `SelfEvalResult`, `TeamworkResult`, `GoalsPD`, `GoalsPP`,`Total`, `Period`, `UserId`)
  VALUES ($one, $two, $three, $four, $five,$six, $period,$id)";

  $this->connect()->exec($sql);
}

public function checkTeamwordAnswerd($period){
  $stmt = $this->connect()->query('select * from answers where questid = 462 and target = '.$this->getUserid().' and period = '.$period);
  if($stmt->rowCount() == 0){
    return true;
}
return false;
}



public function getJuniorAnswers($period){
  $stmt = $this->connect()->query("select count(DISTINCT userid),sum(result) from answers where questid not in (select questid from questions where qid = 16) and userid not in (3,8,11,13,14,18,19) and period = ".$period." and target = ".$this->getuserid());
  $res = $stmt->fetchAll();
  $ret = $res[0][1];
  $ppl = $res[0][0];
  if($ppl == 0){
    return 0;
}
return round($ret/$ppl,0);
}

public function getSupervisorAnswers($period){

  $supers = array(3,8,11,13,14,18,19);
  if (($key = array_search($this->getUserid(), $supers)) !== false) {
    unset($supers[$key]);
}
$m = '';
foreach($supers as $s){
    $m= $m.','.$s;
}
$stmt = $this->connect()->query("SELECT sum(result),count(distinct userid) FROM answers WHERE target = ".$this->getUserId()." and period = ".$period." and userid in(".substr($m,1).") and questid in (select questid from questions where qid = ".$this->getQids()." )");
$res = $stmt->fetchAll();

$ret = $res[0][0];
$ppl = $res[0][1];

return round($ret/$ppl,0);

}

public function getResults($period){
  $stmt = $this->connect()->query("Select * from results where period = ".$period." and userid = ".$this->getUserid());
  $res = $stmt->fetchAll();

  return $res;
}

public function getRersultsWithSort($period){
  $stmt = $this->connect()->query("Select * from results where period = ".$period." order by total Desc");
  $res = $stmt->fetchAll();

  return $res;
}

public function getDataByID($userid){
  $stmt = $this->connect()->query("select firstname,lastname,active from ieduser where userid = ".$userid);
  $res = $stmt->fetch();
  return $res;
}

public function getAllUsers(){
  $stmt = $this->connect()->query("select * from ieduser");
  $res = $stmt->fetchAll();
  return $res;
}

public function createPeriod($participants,$PeriodName,$SDate,$EDate){
  $viewName = str_replace(' ', '', $PeriodName);
  echo '<h4 style = "maring-top:20px;">Period created: '.$viewName;
  $viewName = preg_replace('/[^0-9a-zA-Z_]/',"",$viewName);
  $sql ="Create view $viewName as select * from ieduser where userid in ($participants)";

  try{
    $this->connect()->exec($sql);
}catch(Exception $e){
    echo "There is something wrong with the values you have entered. Please check again";
}
//INSERT INTO `periods` (`PeriodID`, `Participants`, `StartDate`, `EndDate`, `Title`) VALUES ('1', 'kappa', '2019-04-10', '2019-04-26', 'ww');

$stmt = $this->connect()->query("SELECT periodID FROM periods ORDER BY PeriodID DESC LIMIT 1");

$res = $stmt->fetch();

$period = $res[0] + 1;


$sql = "INSERT INTO `periods` (`PeriodID`, `Participants`, `StartDate`, `EndDate`, `Title`) VALUES ('".$period."','".$viewName."', '".$SDate."', '".$EDate."', '".$PeriodName."')";
try{
    $this->connect()->exec($sql);
}catch(Exception $e){
    echo "There is something wrong with the values you have entered. Please check again";
}
}

public function getLastPeriod(){
  $stmt = $this->connect()->query("SELECT periodID FROM periods ORDER BY PeriodID DESC LIMIT 1");

  $res = $stmt->fetch();
  return $res[0];
}

public function isAdmin(){
  $stmt = $this->connect()->query("SELECT Admin from ieduser where lastname = '".$this->getLastname()."'");
  $res = $stmt->fetch();
  if($res[0] == 1){
    return true;
}
return false;
}

public function loadPhoto(){
  switch($this->getUserid()){
    case 1:return "https://ied.eu/entre_files/child-theme/our-team/anna-koronioti.png";break;
    case 2:return "https://ied.eu/entre_files/child-theme/our-team/maria-dalakoura.png";break;
    case 3:return "https://ied.eu/entre_files/child-theme/our-team/panagiotis-koutoudis.png";break;
    case 4:return "https://ied.eu/entre_files/child-theme/our-team/stela-ioannou.png";break;
    case 5:return "https://ied.eu/entre_files/child-theme/our-team/julia-bachousi.png";break;
    case 6:return "https://ied.eu/entre_files/child-theme/our-team/maria-skoufi.png";break;
    case 7:return "https://ied.eu/entre_files/child-theme/our-team/alexandra-chimona.jpg";break;
    case 8:return "https://ied.eu/entre_files/child-theme/our-team/dimitris-siakavelis.png";break;
    case 9:return "https://ied.eu/entre_files/child-theme/our-team/katerina-pariza.png";break;
    case 10:return "https://ied.eu/entre_files/child-theme/our-team/aggeliki-mantse.png";break;
    case 11:return "https://ied.eu/entre_files/child-theme/our-team/anastasios-vasiliadis.png";break;
    case 12:return "https://ied.eu/entre_files/child-theme/our-team/konstantinos-grigoriou.png";break;
    case 13:return "https://ied.eu/entre_files/child-theme/our-team/thodoris-alexandrou.png";break;
    case 14:return "https://ied.eu/entre_files/child-theme/our-team/default-user-image.png";break;
    case 15:return "https://ied.eu/entre_files/child-theme/our-team/eva-mpatzogianni.png";break;
    case 16:return "https://ied.eu/entre_files/child-theme/our-team/katerina-pouspourika.png";break;
    case 17:return "https://ied.eu/entre_files/child-theme/our-team/elissavet-pavlitsa.png";break;
    case 18:return "https://ied.eu/entre_files/child-theme/our-team/mike-lagos.png";break;
    case 19:return "https://ied.eu/entre_files/child-theme/our-team/panos-petsas.png";break;
    case 20:return "https://ied.eu/entre_files/child-theme/our-team/spiros-voulgaris.png";break;
    case 21:return "https://ied.eu/entre_files/child-theme/our-team/vasiliki-soumpenioti.png";break;
    case 22:return "https://ied.eu/entre_files/child-theme/our-team/anastasia-simaioforidou.jpg";break;
    case 23:return "https://ied.eu/entre_files/child-theme/our-team/default-user-image.png";break;
    case 24:return "https://instagram.fath2-1.fna.fbcdn.net/vp/5eaa44dfa47754214ad4e9954c178105/5D69682F/t51.2885-19/s150x150/47581711_506677746521480_266470650196000768_n.jpg?_nc_ht=instagram.fath2-1.fna.fbcdn.net";break;
    case 25:return "https://ied.eu/entre_files/child-theme/our-team/vicky-stathi.jpg";break;
    case 26:return "https://ied.eu/entre_files/child-theme/our-team/eugenia-kosta.jpg";break;
}

}

public function getNotAnswered($period){
  $stmt = $this->connect()->query("select distinct userid from answers where period = $period");
  $res = $stmt->fetchAll();

  $stmt=$this->connect()->query("select participants from periods where periodID = $period");
  $res2 = $stmt->fetch();

  $stmt = $this->connect()->query("select userid from ".$res2[0]);
  $res2 = $stmt->fetchAll();


  $all = array();
  $ans = array();
  foreach($res as $res){
    array_push($ans,$res[0]);
}

foreach($res2 as $res2){
    array_push($all,$res2[0]);
}
echo '<div class = "names_cont">';
$notAnsered = array_diff($all,$ans);
echo '<h2 style = "color:white;text-shadow: -1px 0 black, 0 1
px black, 1px 0 black, 0 -1px black;">The following people must answer: </h2>';
echo "<br>";
foreach($notAnsered as $p){
    $person = $this->getDataByID($p);
    echo '<h4 style = "color:white;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">'.$person[0].' '.$person[1].'</h4>';
}   
echo '</div>';

}


//AFTER NEW REVISIONS. CODE MUST BE DELETED FROM ABOVE. CHECK USAGE
public function checkJSON($value,$period){
  $strJsonFileContents = file_get_contents('answers/period'.$period.'/'.$this->getLastname().$period.'.json');
    $questions = json_decode($strJsonFileContents,true);
    if(isset($questions[$value])){
      echo $value;
      return "true";
    }else{
      return "false";
    }
}

public function insertData1($userid,$target,$answer,$period){
  $sql = "INSERT INTO answers (userID,target,period,Ans1)
     VALUES (".$userid.",'".$target."',".$period.",".$answer.")";
     echo $sql;
   $this->connect()->exec($sql);

}

public function checkAnswered1($period,$target){
    $stmt = $this->connect()->query("select * from answers where UserID = ".$this->getUserid()." and period = ".$period." and target = '".$target."'");
    $result = $stmt->fetchAll();

    if(sizeof($result)!=0){
      return true;
  }else{
      return false;
  }
}

public function checkOtherSupervisor($period){
    $stmt = $this->connect()->query("Select * from answers where target = '".$this->getTextDepartment()."Department' and period = ".$period);
    $result = $stmt->fetchAll();
    echo sizeof($result);
    if(sizeof($result)!=0){
      return true;
  }else{
      return false;
  }

}

}



?>

